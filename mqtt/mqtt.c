
#include "mqtt.h"

//Source de libreria mqtt
//#include "../AT/AT.h"


void checkConnectionErrors(int error){
	switch (error) {
		case MQTT_CONNECTION_ACCEPTED:{
			mqtt.connected = true;
			#if MQTT_DEBUG == 1
				fprintf(pc, "MQTT_CONNECTION_ACCEPTED \r\n" );
			#endif
			break;
		}
		case MQTT_REFUSED_PROTOCOL_VERSION:{
			#if MQTT_DEBUG == 1
				fprintf(pc, "MQTT_REFUSED_PROTOCOL_VERSION \r\n" );
			#endif

			break;
		}
		case MQTT_REFUSED_ID_REJECTED:{
			#if MQTT_DEBUG == 1
				fprintf(pc, "MQTT_REFUSED_ID_REJECTED \r\n" );
			#endif

			break;
		}
		case MQTT_REFUSED_SERVER_UNAVAILABLE:{
			#if MQTT_DEBUG == 1
				fprintf(pc, "MQTT_REFUSED_SERVER_UNAVAILABLE\r\n" );
			#endif

			break;
		}
		case MQTT_REFUSED_BAD_USERNAME_PASS:{
			#if MQTT_DEBUG == 1
				fprintf(pc, "MQTT_REFUSED_BAD_USERNAME_PASS\r\n" );
			#endif

			break;
		}
		case MQTT_REFUSED_NOT_AUTHORIZED:{
			#if MQTT_DEBUG == 1
				fprintf(pc, "MQTT_REFUSED_NOT_AUTHORIZED\r\n" );
			#endif

			break;
		}
		default :
		#if MQTT_DEBUG == 1
			fprintf(pc, "ERROR UNKNOW\r\n" );
		#endif
		break;
	}
}

void mqttReadPacket(char *receivedTopic,char *receivedPayload, int *payloadLen){
		int remaining_len = mqttPacket[0];
			int16 topic_len = make16(mqttPacket[1],mqttPacket[2]);
			#if MQTT_DEBUG == 1
			fprintf(pc, "[MQTT_DEBUG] MQTT_CTRL_PUBLISH\r\n" );
			fprintf(pc, "[MQTT_DEBUG] remainingLength: %d\r\n",remaining_len );
			fprintf(pc, "[MQTT_DEBUG] topicLength: %ld\r\n",topic_len );
			#endif
			char _topic[50];
			char _payload[50];
			int i;
			int j;

			for (i = 0; i < topic_len; i++) {
				_topic[i] = mqttPacket[i+3];
			}
			_topic[i] = 0x00;

			for ( j=i; j < remaining_len; j++) {
				_payload[j - i] = mqttPacket[j+3];
			}

			_payload[j-topic_len-2] = 0x00;
			#if MQTT_DEBUG == 1
			fprintf(pc, "[MQTT_DEBUG] TOPIC: %s\r\n",_topic );
			fprintf(pc, "[MQTT_DEBUG] PAYLOAD: %s\r\n",_payload );
			#endif
			strcpy(receivedTopic,_topic);
			strcpy(receivedPayload,_payload);
			*payloadLen = remaining_len - topic_len - 2;

}

int mqttCheckData(void){
		//DATA_AVAILABLE =false;
		#if MQTT_DEBUG == 1
		//fprintf(pc, "[MQTT_DEBUG] Bytes Available: %ld\r\n", BYTES_AVAILABLE);
		#endif
	char firstbyte = mqtt_rcv_data[0];
	#ifdef _lantronix_h
	if (firstbyte== 'N' || firstbyte == 'D') {
		#if MQTT_DEBUG == 1
		fprintf(pc, "[MQTT_DEBUG] Connection lost :( \r\n" );
		#endif
		mqtt.connected = false;
		return 0;
	}
	#endif

	char CTRL_HEADER = firstbyte >> 4;
	switch (CTRL_HEADER) {
		case MQTT_CTRL_CONNECTACK:{
			#if MQTT_DEBUG == 1
			fprintf(pc, "[MQTT_DEBUG] MQTT_CTRL_CONNECTACK: " );
			#endif
			char connectionCode = mqtt_rcv_data[3]; // posicion 3
			checkConnectionErrors(connectionCode);
			break;
		}
		case MQTT_CTRL_SUBACK:{
			int16 subscriptionCount = make16(mqtt_rcv_data[2],mqtt_rcv_data[3]);
			int qos_level = mqtt_rcv_data[4];
			#if MQTT_DEBUG == 1
			fprintf(pc, "[MQTT_DEBUG] MQTT_CTRL_SUBACK\r\n" );
			fprintf(pc, "[MQTT_DEBUG] Subscription count: %ld\r\n",subscriptionCount );
			fprintf(pc, "[MQTT_DEBUG] QoS: %d\r\n",qos_level );
			#endif

			break;
		}
		case MQTT_CTRL_PUBACK:{
			#if MQTT_DEBUG == 1
			fprintf(pc, "[MQTT_DEBUG] MQTT_CTRL_PUBACK\r\n" );
			#endif
			break;
		}
		case MQTT_CTRL_UNSUBACK:{
			#if MQTT_DEBUG == 1
			fprintf(pc, "[MQTT_DEBUG] MQTT_CTRL_UNSUBACK\r\n" );
			#endif
			break;
		}
		case MQTT_CTRL_PINGRESP:{
			#if MQTT_DEBUG == 1
			fprintf(pc, "[MQTT_DEBUG] MQTT_CTRL_PINGRESP\r\n" );
			#endif
			break;
		}
		case MQTT_CTRL_PUBLISH:{

			int remainingLength = mqtt_rcv_data[1];
			int16 topicLength = make16(mqtt_rcv_data[2],mqtt_rcv_data[3]);
			#if MQTT_DEBUG == 1
			fprintf(pc, "[MQTT_DEBUG] MQTT_CTRL_PUBLISH\r\n" );
			fprintf(pc, "[MQTT_DEBUG] remainingLength: %d\r\n",remainingLength );
			fprintf(pc, "[MQTT_DEBUG] topicLength: %ld\r\n",topicLength );
			#endif
			for(int i = 0; i <remainingLength+1; i++ ){
					mqttPacket[i] = mqtt_rcv_data[i+1];
			}
			
			/*char receivedTopic[30];
			char receivedPayload[30];
			int i;

			for (i = 0; i < topicLength; i++) {
				receivedTopic[i] = replybuffer[i+4];
			}
			receivedTopic[i+1] = 0x00;

			for ( int j = 0; j < remainingLength; j++) {
				receivedPayload[j] = replybuffer[4+i+j];
			}

			receivedPayload[j+1] = 0x00;
			#if MQTT_DEBUG == 1
			fprintf(pc, "[MQTT_DEBUG] TOPIC: %s\r\n",receivedTopic );
			fprintf(pc, "[MQTT_DEBUG] PAYLOAD: %s\r\n",receivedPayload );
			#endif
*/

			break;
		}
		default:{
		#if MQTT_DEBUG == 1
		fprintf(pc, "[MQTT_DEBUG] Dato desconocido\r\n" );
		#endif
		break;
		}
	}
return CTRL_HEADER;
}
char *stringprint(char *p,   char *s, int16 maxlen=0) {
	// If maxlen is specified (has a non-zero value) then use it as the maximum
	// length of the source string to write to the buffer.  Otherwise write
	// the entire source string.
	int16 len = strlen(s);
	if (maxlen > 0 && len > maxlen) {
		len = maxlen;
	}
	/*
	for (uint8_t i=0; i<len; i++) {
		Serial.write(pgm_read_byte(s+i));
	}
	*/
	p[0] = len >> 8; p++;
	p[0] = len & 0xFF; p++;
	strncpy((char *)p, s, len);
	return p+len;
}


int connectPacket(char *packet){
    memset(mqtt_rcv_data, 0, sizeof(mqtt_rcv_data));
	char *p = packet;
	//char protocol[10];
	int16 len;
	p[0] = (MQTT_CTRL_CONNECT << 4)| 0x00;
	p+=2;
	#if MQTT_PROTOCOL_LEVEL == 4
		//strcpy(protocol,"MQTT");
		p = stringprint(p,(char *)"MQTT");

    #endif
        //#error MQTT level not supported
	//#endif
	p[0] = MQTT_PROTOCOL_LEVEL;
	p++;
	p[0] = MQTT_CONN_CLEANSESSION| MQTT_CONN_USERNAMEFLAG | MQTT_CONN_PASSWORDFLAG;
	p++;
	p[0] = MQTT_CONN_KEEPALIVE >> 8;
	p++;
	p[0] = MQTT_CONN_KEEPALIVE & 0xFF;
	p++;

	p = stringprint(p,&mqtt.clientid);
	p = stringprint(p,&mqtt.username);
	p = stringprint(p,&mqtt.password);

	len = p - packet;
	packet[1] = len-2;

	return len;

}
void mqttCredentials(char *userName, char *passWord,char *userId){
    strcpy(mqtt.username,userName);
    strcpy(mqtt.password,passWord);
    strcpy(mqtt.clientid,userId);
    mqtt.connected = false;
}
boolean mqttConnect(char *server, int16 port){
	MQTT_SUB_COUNT = 0; // reseteamos el contador de subscripcion
	strcpy(mqtt.server,server);
	mqtt.port = port;

	#ifdef _ESP32_H
    
	if(!tcp_start_connection(TCP_TYPE,mqtt.server,mqtt.port)){
		#if MQTT_DEBUG == 1
		fprintf(pc,"[MQTT_DEBUG] Error en conexion\r\n");
		#endif
		return false;
	}
	#if MQTT_DEBUG == 1
	fprintf(pc,"[MQTT_DEBUG] Conectado a: %s:%ld\r\n",mqtt.server,mqtt.port);
	#endif

	#endif
	#ifdef _lantronix_H

	if (!CONNECT_SERVER(&mqtt.server,mqtt.port)) {
		return false;
	}
	#endif
	delay_ms(500);
	int bytes_to_send = connectPacket(&mqttPacket);

	#ifdef _ESP32_H
    at_config.end_line = FALSE;
	if(!tcp_send_data(&mqttPacket,bytes_to_send)){
		#if MQTT_DEBUG == 1
		fprintf(pc,"[MQTT_DEBUG] Error en envio!\r\n");
		#endif

		return false;

		//fprintf(pc,"[MAIN] Send data SUCCESS!\r\n");
    }
	
    
    esp32_mqtt_event = ESP32_event_available(BYTE_TYPE);
    at_config.end_line = FALSE;
	while (esp32_mqtt_event == NULL_EVENT){
        esp32_mqtt_event = ESP32_event_available(BYTE_TYPE);// importante para recibir bytes
    }
	if (esp32_mqtt_event != TCP_EVENT){
		return false;
	}
	if (tcp.response != RECEIVE_NETWORK_DATA)
	{
		return false;
	}
	
	//strcpy(mqtt_rcv_data,tcp.rcv_data);
    bufferncpy(mqtt_rcv_data,tcp.rcv_data,tcp.rcv_data_len);
	
	#endif

	#ifdef _lantronix_H
	sendLantronix(&mqttPacket,bytes_to_send);
	//#endif
	if (timeout_data(5000)) {
		return false;
	}
	#endif
	if (mqttCheckData() != MQTT_CTRL_CONNECTACK) {
		mqtt.connected = false;
		return false;
	}
	if (!mqtt.connected) {
		return false;
	}
	return true;

}
int publishPacket(char *packet, char *_topic, char *_payload, int payload_len){
	char *p = packet;
	int16 len;
	p[0] = (MQTT_CTRL_PUBLISH << 4)| 0x00;
	p+=2;
	p = stringprint(p,_topic);
	strncpy((char *)p, _payload, payload_len);
	p+=payload_len;
	//strncpy(p[0],_payload,payload_len);

	len = p - packet;
	packet[1] = len-2;

	return len;

}
int subscribePacket(char *packet, char *_topic){
	char *p = packet;
	int16 len;
	p[0] = (MQTT_CTRL_SUBSCRIBE << 4)| 0x02;
	p+=2;
	p[0] = 0x00;
	p++;
	p[0] = MQTT_SUB_COUNT++; //contador de packets
	p++;
	p = stringprint(p,_topic);
	//p++ ;
	p[0] = 0x00 ;// flags ??
	p++;
	//strncpy(p[0],_payload,payload_len);

	len = p - packet;
	packet[1] = len-2;

	return len;

}
boolean mqttPublish(char *topic, char *payload, int len){
	int bytes_to_send = publishPacket(&mqttPacket,topic,payload,len);

	#ifdef _ESP32_H
	if(!tcp_send_data(&mqttPacket,bytes_to_send)){
		return FALSE;
	}
	#endif

	#ifdef _lantronix_h
	sendLantronix(&mqttPacket,bytes_to_send);
	/*if (timeout_data(5000)) {
		return false;
	}
	mqttCheckData();*/
	#endif
	return true;
}

boolean mqttSubscribe(char *topic){
	int bytes_to_send = subscribePacket(&mqttPacket,topic);
	#ifdef _ESP32_H
	if(!tcp_send_data(&mqttPacket,bytes_to_send)){
		return FALSE;
	}
	esp32_mqtt_event = ESP32_event_available(BYTE_TYPE);
	while (esp32_mqtt_event == NULL_EVENT){
        esp32_mqtt_event = ESP32_event_available(BYTE_TYPE);
    }
	if (esp32_mqtt_event != TCP_EVENT){
		return false;
	}
	if (tcp.response != RECEIVE_NETWORK_DATA)
	{
		return false;
	}
	
	//strcpy(mqtt_rcv_data,tcp.rcv_data);
    bufferncpy(mqtt_rcv_data,tcp.rcv_data,tcp.rcv_data_len);

	#endif
	#ifdef _lantronix_
	sendLantronix(&mqttPacket,bytes_to_send);
	if (timeout_data(5000)) {
		return false;
	}
	#endif
	if(mqttCheckData() != MQTT_CTRL_SUBACK){
			return false;
	}
	return true;
}

boolean mqttSendPing(void){
	mqttPacket[0] = (MQTT_CTRL_PINGREQ << 4)| 0x00;
	mqttPacket[1] = 0x00; //remain length

	#ifdef _ESP32_H
	
	if(!tcp_send_data(&mqttPacket,2)){
		mqtt.connected = false;
		return FALSE;
	}
    esp32_mqtt_event = ESP32_event_available(BYTE_TYPE);
	while ( esp32_mqtt_event == NULL_EVENT){
        esp32_mqtt_event = ESP32_event_available();
    }
    
	if (esp32_mqtt_event != TCP_EVENT){
		mqtt.connected = false;
		return false;
	}
	if (tcp.response != RECEIVE_NETWORK_DATA)
	{
		return false;
	}
	
	bufferncpy(mqtt_rcv_data,tcp.rcv_data,tcp.rcv_data_len);



	#endif


	#ifdef _lantronix_h
	sendLantronix(&mqttPacket,2);
	if (timeout_data(5000)) {
		mqtt.connected = false;
		return false;
	}
	#endif
	if (mqttCheckData() != MQTT_CTRL_PINGRESP) {
		mqtt.connected = false;
		return false;
	}
	return true;

}
boolean mqttPacketAvailable(void){
	#ifdef _ESP32_H
    
	switch(tcp.response){
		case RECEIVE_NETWORK_DATA:
		mqtt.connected = true;
		bufferncpy(mqtt_rcv_data,tcp.rcv_data,tcp.rcv_data_len);
		return true;
		break;
		case CLOSED:
		mqtt.connected = false;
		return false;
		break;
		
		
	}
	return false;
	#endif
		//return DATA_AVAILABLE;
}


boolean mqttClientConnected(void){
		return mqtt.connected;

}
