/*
 * Project: AT
 * Description: Libreria para manejar el envio y recepcion de comandos AT
 * Author: Arturo Gasca
 * Date:  Julio-2019
 * Development: Tuna Shields
 */

//AT_config.multiline = false;
//AT_config.end_line = true;

#if (RADIO_SERIAL_SOURCE == RADIO_INT_RDA)
#int_rda
#elif (RADIO_SERIAL_SOURCE == RADIO_INT_RDA2)
#int_rda2
#elif (RADIO_SERIAL_SOURCE == RADIO_INT_RDA3)
#int_rda3
#elif (RADIO_SERIAL_SOURCE == RADIO_INT_RDA4)
#int_rda4
#endif
void radio_receive(){
	int16 timeout = 1000;

	int16 replyidx = 0;
	int16 timeoutBack = timeout;
	while (timeout--) {
			if (replyidx >= BUFFER_SIZE) {
				break;
			}

			if(kbhit(radio)) {

				timeout = timeoutBack;
				char c =  fgetc(radio);
				if(at_config.end_line){
					if (c == '\r') continue;
					if (c == 0xA) {
							if (replyidx == 0)   // the first 0x0A is ignored
									continue;

							if (!at_config.multiline) {
									timeout = 0;         // the second 0x0A is the end of the line
									break;
							}
                            
					}

				}

				at.replybuffer[replyidx] = c;
				replyidx++;
			}

			if (timeout == 0) {
				break;
			}
			delay_us(10);
	}
	at.replybuffer[replyidx] = 0;  // null term
	at.data_available = true;
	at.reply_len = replyidx;
	
}



void radio_reset(void){

	output_high(RADIO_RST);
	delay_ms(200);
	output_low(RADIO_RST);
	delay_ms(500);
	output_high(RADIO_RST);
	delay_ms(200);
}



BOOLEAN timeout_data(int16 milis_timeout){ // cambio en timeout
	int16 timeout_ms = 0;
	while (! at.data_available && milis_timeout>timeout_ms  ) {
		timeout_ms++;
		delay_ms(1);
	}
	if (at.data_available) {
		#if RADIO_DEBUG == 1
			fprintf(pc, "[AT_DEBUG] Dato disponible!\r\n" );
		#endif

            //delay_ms(1000); // se grega tiempo para esperar que se almacenen los datos de manera correcta
            return false;
	}else if (timeout_ms >= milis_timeout) {
		#if RADIO_DEBUG == 1
			fprintf(pc, "[AT_DEBUG] Timeout int serial\r\n" );
		#endif
            return true;
	}
}




/******************* LOW LEVEL ************************************************/
void flushInput() {
	// Read all available serial input to flush pending data.
	//memset(replybuffer,0,sizeof(replybuffer));
	int16 timeoutloop = 0;
	while (timeoutloop++ < 40) {
		while(kbhit(radio)) {
			getc(radio);
			timeoutloop = 0;  // If char was received reset the timer
		}
		delay_ms(1);
}
}

int16 readRaw(int16 b) {
	int16 idx = 0;

	while (b && (idx < sizeof(at.replybuffer)-1)) {
		if (kbhit(radio)) {
			at.replybuffer[idx] = getc(radio);
			idx++;
			b--;
		}
	}
	at.replybuffer[idx] = 0;

	return idx;
}


int getReply( char* send, int16 timeout,BOOLEAN multiline,BOOLEAN endline) {
    at_config.end_line = endline;
    at_config.multiline = multiline;
	fprintf(radio,"%s\r\n",send );
	if(timeout_data(timeout)){
		#if RADIO_DEBUG == 1
        fprintf(pc,"[AT_ERROR] TIMEOUT SERIAL\r\n");
		#endif
    }else{
        #if RADIO_DEBUG == 1
		fprintf(pc,"[AT_DEBUG] \t<--- %s\r\n", at.replybuffer);
		#endif
    }
	at.data_available = false; // lo pongo en false por que ya tengo el dato que requiero en replybuffer
	//int l = readline(timeout);
	return at.reply_len;
}

boolean sendCheckReply( char *send, char *reply, int16 timeout,BOOLEAN multiline,BOOLEAN endline){
    at_config.end_line = endline;
    at_config.multiline = multiline;
	#if RADIO_DEBUG == 1
		fprintf(pc, "[AT_DEBUG]  SEND: %s\r\n",send );
		fprintf(pc, "[AT_DEBUG] REPLY: %s\r\n",reply );
	#endif
	getReply(send,timeout);
	return (strcmp(at.replybuffer, reply) == 0);
}

boolean sendParseReply(char* tosend,  char* toreply, int16 *v, char divider, int index,BOOLEAN multiline,BOOLEAN endline) {

	getReply(tosend,DEFAULT_TIMEOUT_MS,multiline,endline);

		if (! parseReply(toreply, v, divider, index)) return false;

		//readline(MODEM_DEFAULT_TIMEOUT_MS); // eat 'OK'

		return true;
}

boolean sendParseReplyChar(char* tosend,  char* toreply, char *v, char divider, int index,BOOLEAN multiline,BOOLEAN endline) {
		getReply(tosend,DEFAULT_TIMEOUT_MS,multiline,endline);

		if (! parseReplychar(toreply, v, divider, index)) return false;

		//readline(MODEM_DEFAULT_TIMEOUT_MS); // eat 'OK'

		return true;
}

boolean parseReply( char* toreply, int16 *v, char divider, int8 index) {
		char *p = strstr(at.replybuffer, toreply);  // get the pointer to the voltage
		if (p == 0) return false;
		p += stringLEN(toreply);

		for (int i=0; i<index;i++) {
				// increment dividers
				p = strchr(p, divider);
				if (!p) return false;
				p++;
		}
		char num[5];

		strcpy(num,p);

		*v = atol(p);


		#if RADIO_DEBUG == 1
            int16 numerito = atol(p);
			fprintf(pc, "[AT_DEBUG] Toreply: %s\r\n",toreply);
			fprintf(pc, "[AT_DEBUG] index: %d\r\n",index );
			fprintf(pc, "[AT_DEBUG] Divider: %c\r\n",divider);
			fprintf(pc, "[AT_DEBUG] Result: %ld\r\n",numerito);
		#endif

		return true;
}


boolean parseReplyChar( char* toreply, char *v, char divider  = ',', int index=0){
	long i=0;
		char *p = strstr(at.replybuffer, toreply);
		if (p == 0) return false;
		p+=stringLEN(toreply);

		for (i=0; i<index;i++) {
				// increment dividers
				p = strchr(p, divider);
				if (!p) return false;
				p++;
		}

		for(i=0; i<stringLEN(p);i++) {
				if(p[i] == divider)
						break;
				v[i] = p[i];
		}

		v[i] = '\0';

		#if RADIO_DEBUG == 1
		fprintf(pc, "[AT_DEBUG] Toreply: %s\r\n",toreply);
		fprintf(pc, "[AT_DEBUG] index: %d\r\n",index );
		fprintf(pc, "[AT_DEBUG] Divider: %c\r\n",divider);
		fprintf(pc, "[AT_DEBUG] Result: %s\r\n",v);
		#endif

		return true;
}

boolean parseReplyElements( int elements,char* toreply, char *v, char divider  = ',', int index=0){
	int i=0;
		char *p = strstr(at.replybuffer, toreply);
		if (p == 0) return false;
		p+=stringLEN(toreply);

		/*for (i=0; i<index;i++) {
				// increment dividers
				p = strchr(p, divider);
				if (!p) return false;
				p++;
		}*/

		for(i=0; i<elements;i++) {
				//if(p[i] == divider)
						//break;
				v[i] = p[i];
		}

		//v[i] = '\0';

		#if RADIO_DEBUG == 1
		fprintf(pc, "[AT_DEBUG] Toreply: %s\r\n",toreply);
		fprintf(pc, "[AT_DEBUG] index: %d\r\n",index );
		fprintf(pc, "[AT_DEBUG] Divider: %c\r\n",divider);
		fprintf(pc, "[AT_DEBUG] Result: [");
        int e;
        for (e = 0; e < elements; e++) {
            fprintf(pc, "%X",v[e]);
        }
        fprintf(pc, "]\r\n");

		#endif

		return true;
}


void clean_buffer(void){
     memset(at.replybuffer, 0, sizeof(at.replybuffer));
     memset(at.command, 0, sizeof(at.command));
     memset(at.response, 0, sizeof(at.response));

}

void send_raw_data(char *data, int16 len){
	//fprintf(radio,"%s",data);
    int i;
    for (i = 0; i < len; i++) {
        fputc(data[i],radio);

    }

}


BOOLEAN string_contains(char*expecting, char*received){

	if (strstr(received,expecting) == 0){
		return false;
	}
	else{
		return true;
	}

}

int bufferncpy(unsigned char *dest, unsigned char *scr, size_t n){
	for (int i = 0; i < n; i++)
    {
        *(dest + i) = *(scr + i);
    }
	return n;
}
int16 stringLEN(char *s){
    int16 i = 0;
    int16 len = 0;

    while (s[i] != '\0'){
        len++;
        i++;
    }

    return len;
 }