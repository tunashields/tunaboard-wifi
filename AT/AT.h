/*
Libreria generica para el manejo de comandos AT mediante UART
Usar las definiciones desde archivo main, antes de llamar la libreria
para que la compilacion sea correcta
RADIO_RST               --- pin reset modulo
RADIO_RX                --- pin recepcion desde el radio
RADIO_TX                --- pin transmision al radio
RADIO_BAUD              --- baudrate 9600,115200 etc
DEBUG                   --- 1 activa, 0 desactiva
RADIO_SERIAL_SOURCE     --- RADIO_INT_RDA, RADIO_INT_RDA2...RDA4
*/

#ifndef _AT_H
#define _AT_H

//*********************************DEFINIR PINES Y VELOCIDAD DEL RADIO
#ifndef RADIO_RST
#define RADIO_RST   PIN_E1  //radio reset pin
#define RADIO_RX    PIN_D7  //pic receive from radio
#define RADIO_TX    PIN_D6  //pic transmit to radio
#define RADIO_BAUD  115200  //baudrate
#endif
#ifndef DEBUG   //1 habilita debug, 0 deshabilita debug
#define DEBUG 1
#endif

#define RADIO_INT_RDA     	77777
#define RADIO_INT_RDA2		66666
#define RADIO_INT_RDA3    	44444
#define RADIO_INT_RDA4    	33333
#ifndef RADIO_SERIAL_SOURCE 
#define RADIO_SERIAL_SOURCE RADIO_INT_RDA
#endif



//Defines para uso de libreria
#define BUFFER_SIZE 500
#define DEFAULT_TIMEOUT_MS 1000
#define RADIO_DEBUG DEBUG 

#use rs232(baud=RADIO_BAUD,parity=N,xmit=RADIO_TX,rcv=RADIO_RX,bits=8,stream=radio)

//Variables para buffer
/*
char replybuffer[BUFFER_SIZE];
int16 reply_len = 0;
char AT[150] ; //Comando AT para enviar
char RESP[50]; //Respuesta esperada
int DATA_AVAILABLE  = FALSE;*/

struct AT_VAR
{
    char command[150];  //Comando AT para enviar
    char response[100]; //Respuesta esperada
    char data_available; //flag de respuesta recibida
    long reply_len;     //Cantidad de bytes en la respuesta
    char replybuffer[BUFFER_SIZE]; //Buffer de recepcion
    
}at;


/*
struct at_configuration
{
    int multiline;
    int end_line;
}at_config;
*/
struct ATCONFIGURATION{
    BOOLEAN multiline;
    BOOLEAN end_line;
}at_config;





//#include "stdint.h"
/*
send = Puntero de comando para enviar
reply = respuesta esperada del comando
timeout = tiempo de respuesta del comando
 */
boolean sendCheckReply(char *send, char *reply, int16 timeout = DEFAULT_TIMEOUT_MS,BOOLEAN multiline = false,BOOLEAN endline = true );
boolean sendCheckReplySuffix(char* prefix, int32 suffix,  char* reply, int16 timeout = DEFAULT_TIMEOUT_MS) ;
boolean sendCheckReplyQuoted( char* prefix,  char* suffix,  char* reply, unsigned int16 timeout = DEFAULT_TIMEOUT_MS);
/*
Envia comando y parsea la respuesta siempre y cuando sea numero... y con divisor
El divider por default es ','
tosend = Puntero de comando para enviar
toreply = respuesta esperada del comando
v = puntero donde se almacena la respuesta
divider = separador
index =indice de separador en donde se encuentra la respuesta esperada
 */
boolean sendParseReply( char* tosend,  char* toreply, int16 *v, char divider = ',', int index = 0,BOOLEAN multiline = false,BOOLEAN endline = true);
/*
Envia comando y parsea la respuesta siempre y cuando sea numero... y con divisor
El divider por default es ','
tosend = Puntero de comando para enviar
toreply = respuesta esperada del comando
v = puntero donde se almacena la respuesta
divider = separador
index =indice de separador en donde se encuentra la respuesta esperada
 */
boolean sendParseReplyChar(char* tosend,  char* toreply, char *v, char divider = ',', int index = 0,BOOLEAN multiline = false,BOOLEAN endline = true);
/*
Parsea una respuesta ya almacenada en "replybuffer" debe ser numero..
El divider por default es ','
toreply = respuesta esperada del comando
v = puntero donde se almacena la respuesta
divider = separador
index =indice de separador en donde se encuentra la respuesta esperada
 */
boolean parseReply( char* toreply, int16 *v, char divider = ',', int8 index);
/*
Parsea la respuesta almacenada en replybuffer siempre y cuando sea un string... y con divisor
El divider por default es ','
toreply = respuesta esperada del comando
v = puntero donde se almacena la respuesta
divider = separador
index =indice de separador en donde se encuentra la respuesta esperada
 */
boolean parseReplyChar( char* toreply, char *v, char divider  = ',', int index=0);
/*
 *Parsea la respuesta almacenada en replybuffer sin tomar el cuenta el nNULL
 * El divider por default es ','
 * elements =  cantidad de elementos a copiar, a partir de la respuesta espera y el divider..
 * toreply = respuesta esperada del comando
 * v = puntero donde se almacena la respuesta
 * divider = separador
 * index =indice de separador en donde se encuentra la respuesta esperada
 */
boolean parseReplyElements( int elements,char* toreply, char *v, char divider  = ',', int index=0);
/*
Compara una respuesta esperada previamente almacenada en replybuffer
reply = respuesta esperada del comando
 */
boolean expectReply( char* reply, unsigned int16 timeout = DEFAULT_TIMEOUT_MS);

//int readline(int16 timeout = DEFAULT_TIMEOUT_MS, boolean multiline = false);


/*
Envia un comando AT
send =  comando se agrega automatico \r\n
Regresa la cantidad de bytes de la respuesta
 */
int getReply( char* send, int16 timeout = DEFAULT_TIMEOUT_MS,BOOLEAN multiline = false,BOOLEAN endline = true);
/*
Funcion para esperar la respuesta del comando
Se queda esperando la respuesta y regresa 1 si hay timeout
 */
boolean timeout_data(int16 milis_timeout);
/*
Limpia puerto serie
 */
void flushInput();
/*
* Funcion para recibir tramas en la interrupcion del PIC, coloar en #RDA
*/
void radio_receive();
/*
Limpia variables de RAM
 */
void clean_buffer(void);

/*
Envia un comando AT en raw
 */
void send_raw_data(char *data, int16 len);


/**
 * Compare 2 strings
 */
BOOLEAN string_contains(char*expecting, char*received);
/**
 * Copy buffer scr in dest, sin importar el null.. size n
 */
int bufferncpy(unsigned char *dest, unsigned char *scr, size_t n);

/**
 * Sustituye a strlen por que solo puede 255
 */
int16 stringLEN(char *s);

#include "AT.c"


#endif //
