
/*****************************WIFI FUNCTIONS******************* */



BOOLEAN wifi_mode(int mode){

	sprintf(at.command,"AT+CWMODE=%d",mode);
	strcpy(at.response,"OK");

	if (!sendCheckReply(at.command,at.response,DEFAULT_TIMEOUT_MS)){
		return false;
  	}
	return true;
}



BOOLEAN wifi_connect(char *ssid, char *pwd){
	wifi.status = WIFI_DISCONNECTED;
    clean_buffer();
	sprintf(at.command,"AT+CWJAP=\"%s\",\"%s\"",ssid,pwd);
    //at_config.multiline = true; //multiline en caso de error manda los comandos muy rapido
	getReply(at.command,30000,1); //espero 30 seg
	at.data_available = true; // asumo la DATA_AVAILABLE para entrar a analizar con ESP32_event_available()

	ESP_event_available(); //checo el evento, aqui parseo y guardo en wifi.status
    //at_config.multiline = false; // deshabilito multiline para futuros comandos
    at.data_available = false; // deshabilito bandera, ya tengo mi respuesta almacenada

	if (wifi.status != WIFI_CONNECTED) // si mi estado es distinto de conectedado.. regresa error
	{
        return false;
	}
    while(ESP_event_available() == NULL_EVENT); //espero la siguiente respuesta
    if (wifi.last_event != WIFI_GOT_IP) // tengo ip?
	{
        return false;
	}
    timeout_data(2000); // espero el ultimo "OK"
    strcpy(at.response,"OK");
    if(!string_contains(at.response,at.replybuffer)){
        return false;
    }

    return true;
}

BOOLEAN wifi_disconnect(void){
	strcpy(at.command,"AT+CWQAP");
	strcpy(at.response,"OK");

	if (!sendCheckReply(at.command,at.response,DEFAULT_TIMEOUT_MS)){
		return false;
  	}
	  wifi.status = WIFI_DISCONNECTED;
	return true;
}


BOOLEAN wifi_show_available_ap(void){
	strcpy(at.command,"AT+CWLAP");
	strcpy(at.response,"OK");

	if (!sendCheckReply(at.command,at.response,DEFAULT_TIMEOUT_MS)){
		return false;
  	}
	return true;
}

BOOLEAN wifi_auto_connect(boolean mode){
    sprintf(at.command,"AT+CWAUTOCONN=%d",mode);
	strcpy(at.response,"OK");

	if (!sendCheckReply(at.command,at.response,DEFAULT_TIMEOUT_MS)){
		return false;
  	}
	return true;
}

BOOLEAN wifi_get_info(char *ip, char *gw, char *netmsk){
    clean_buffer();
	//at_config.multiline = true;
	strcpy(at.command,"AT+CIPSTA?");
	getReply(at.command,DEFAULT_TIMEOUT_MS,1);
	strcpy(at.response,"+CIPSTA:ip:");
	parseReplyChar(at.response,ip,'"',1);
	strcpy(at.response,"+CIPSTA:gateway:");
	parseReplyChar(at.response,gw,'"',1);
	strcpy(at.response,"+CIPSTA:netmask:");
	parseReplyChar(at.response,netmsk,'"',1);
	//at_config.multiline = false;
	return true;
}
/*********************************************** */
