#ifndef _wifi_H
#define _wifi_H


#define WIFI_STATION        1 //Modo wifi normal
#define WIFI_SOFTAP         2 //Modo wifi access point
#define WIFI_STATION_SOFTAP 3 //Modo wwifi normal+accesspoint


#define OPEN            0
#define WEP             1 //not supported for SoftAP
#define WPA_PSK         2
#define WPA2_PSK        3
#define WPA_WPA2_PSK    4

enum responses{
    WIFI_CONNECTED,
    WIFI_GOT_IP,
    WIFI_DISCONNECTED,
    WIFI_CONNECTION_TIMEOUT,
    WIFI_WRONG_PASSWORD,
    WIFI_CANNOT_FIND_AP,
    WIFI_CONNECTION_FILED,
    WIFI_ERROR_UNKNOWN,
    WIFI_NULL_EVENT
    
}event;
struct ESP32
{
    enum responses last_event;
    enum responses status;
    int16 respuesta;
    char ip[20];
    char gateway[20];
    char netmask[20];
}wifi;

/*
*Sets te Wi-fi mode 
WIFI_STATION
WIFI_SOFTAP
WIFI_STATION_SOFTAP
*/
BOOLEAN wifi_mode(int mode);
/*
Conecta a un AP con SSID y PASS, previo se debe cambiar usar wifi_mode(WIFI_STATION)
 */
BOOLEAN wifi_connect(char *ssid, char *pwd);

/*
Desconecta del AP
 */
BOOLEAN wifi_disconnect(void);
/*
Configuracion del SoftAP ssid 
ssid: network name
pwd: password
channel: wifi channel
encrypt: OPEN,WPA_PSK,WPA2_PSK,WPA_WPA2_PSK
 */

/*
 */
BOOLEAN wifi_show_available_ap(void);

/*
*/
BOOLEAN wifi_auto_connect(boolean mode);

/*
 */

/*
 */
BOOLEAN wifi_get_info(char *ip, char *gw, char *netmsk);

/*
 */
BOOLEAN softAP_set_config(char *ssid, char *pwd, int channel, int encrypt);

#include "wifi.c"

#endif