#ifndef _ESP32_H
#define _ESP32_H

#define ESP_RST   PIN_B5
#define ESP_RX    PIN_D7
#define ESP_TX    PIN_D6
#define ESP_BAUD  115200
enum rcv_data_type{
    BYTE_TYPE,
    CHAR_TYPE
};

enum event_type{
    NULL_EVENT,
    WIFI_EVENT,
    TCP_EVENT,
    ESP_EVENT
};


/*
Revisa si hay evento disponible
 */
enum event_type ESP32_event_available(rcv_data_type net_data_type = CHAR_TYPE);

/*
* reinicia modulo y verifica el comando de antencion
 */
boolean ESP32_init(boolean hw_reset = true);
/*
* Print ESP32 version 
*/
void ESP32_version(void);
/*
ESP32 enters Deep-sleep mode
ESP32 will wake up after Deep-sleep for as many milliseconds as <time>
indicates
 */
BOOLEAN EP32_sleep(int16 time);
/*
Restaura el ESP a los valores predeterminados
 */

BOOLEAN ESP32_restore(void);


#include "ESP32.c"
#endif //

