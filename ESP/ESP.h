#ifndef _ESP_H
#define _ESP_H

#ifndef ESP_ENABLE
#define ESP_ENABLE PIN_A0
#endif

enum rcv_data_type{
    BYTE_TYPE,
    CHAR_TYPE
};

enum event_type{
    NULL_EVENT,
    WIFI_EVENT,
    TCP_EVENT,
    ESP_EVENT
};


/*
Revisa si hay evento disponible
 */
enum event_type ESP_event_available(rcv_data_type net_data_type = CHAR_TYPE);

/*
* reinicia modulo y verifica el comando de antencion
 */
boolean ESP_init(boolean hw_reset = true);
/*
* Print ESP32 version 
*/
void ESP_version(void);
/*
ESP32 enters Deep-sleep mode
ESP32 will wake up after Deep-sleep for as many milliseconds as <time>
indicates
 */
BOOLEAN ESP_sleep(int16 time);
/*
Restaura el ESP a los valores predeterminados
 */

BOOLEAN ESP_restore(void);


#include "ESP.c"
#endif //

