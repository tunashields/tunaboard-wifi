/*
 * Project: ESP32 v1
 * Description: Libreria para modulo ESP32 para PIC CCS
 * Author: Arturo Gasca
 * Date:  Julio-2019
 * Development: Tuna Shields
 */

/*#define RADIO_RST   ESP_RST
#define RADIO_RX    ESP_RX
#define RADIO_TX    ESP_TX
#define RADIO_BAUD  115200*/
//#include "../AT/AT.h"  // manejador de puerto serie y comandos seriales

#include "wifi/wifi.h"		//funciones WIFI del ESP
#include "tcp/tcp.h"	// funciones TCP/IP del ESP






/*********************ESP32 FUNCTIONS******************************* */

enum event_type ESP_event_available(enum rcv_data_type net_data_type = CHAR_TYPE){
    if(net_data_type == BYTE_TYPE){
        at_config.end_line = FALSE; //Se pone en false cuando vamos a estar recibiendo hexadecimales desde TCP
    }else{
        at_config.end_line = TRUE; //Se pone en false cuando vamos a estar recibiendo hexadecimales desde TCP
    }
    if (!at.data_available)
    {
        return NULL_EVENT;
    }
	at.data_available = false;
	#if RADIO_DEBUG == 1
	fprintf(pc, "[ESP32_EVENT] <-- %s\r\n",at.replybuffer );
	#endif

    strcpy(at.response,"ready");
	if (string_contains(at.response,at.replybuffer)){		        
		return ESP_EVENT;
        
	}

	strcpy(at.response,"WIFI CONNECTED");
	if(strstr(at.replybuffer, at.response) != 0){
		wifi.status = WIFI_CONNECTED;
		wifi.last_event = WIFI_CONNECTED;
        
		return WIFI_EVENT;
        
	}
	strcpy(at.response,"WIFI GOT IP");
	if(strstr(at.replybuffer, at.response) != 0){
		wifi.last_event = WIFI_GOT_IP;
        wifi.status = WIFI_GOT_IP;
		return WIFI_EVENT;
	}
	strcpy(at.response,"WIFI DISCONNECT");
    if (string_contains(at.response,at.replybuffer)){		        
		wifi.status = WIFI_DISCONNECTED;
		wifi.last_event = WIFI_DISCONNECTED;
		return WIFI_EVENT;
        
	}
    strcpy(at.response,"+CWJAP:");
	if(strstr(at.replybuffer, at.response) != 0){

		
		parseReply(at.response, &wifi.respuesta, , 0);

		switch (wifi.respuesta)
		{
		case 1:
			wifi.last_event = WIFI_CONNECTION_TIMEOUT;
			break;
		case 2:
			wifi.last_event = WIFI_WRONG_PASSWORD;
			break;
		case 3:
			wifi.last_event = WIFI_CANNOT_FIND_AP;
			break;
		case 4:
			wifi.last_event = WIFI_CONNECTION_FILED;
			break;
		default:
			wifi.last_event = WIFI_ERROR_UNKNOWN;
			break;
		}
		
		return WIFI_EVENT;
	}
    strcpy(at.response,"+IPD,");
    if (string_contains(at.response,at.replybuffer)) { //Llego dato de la red
        
        parseReply(at.response,&tcp.rcv_data_len,':', 0);
        sprintf(at.response,"+IPD,%ld:",tcp.rcv_data_len);
        if(net_data_type == BYTE_TYPE){
            parseReplyElements(tcp.rcv_data_len,at.response,&tcp.rcv_data,NULL,0); //parseamos sin tomar en cuenta el NULL y le decimos cuantos bytes copie
           
        }
        else if(net_data_type == CHAR_TYPE){
            parseReplyChar(at.response,&tcp.rcv_data,NULL,0); // por default entra aqui, copia tomando en cuenta el NULL
           
        }
        
        tcp.data_available = true;
        tcp.response = RECEIVE_NETWORK_DATA;
        return TCP_EVENT;

    }
    strcpy(at.response,"CONNECT");
    if(string_contains(at.response,at.replybuffer)){
        tcp.response = CONNECT;
        return TCP_EVENT;
    }
    strcpy(at.response,"CLOSED");
    if(string_contains(at.response,at.replybuffer)){
        tcp.response = CLOSED;
        return TCP_EVENT;
    }

	wifi.last_event = WIFI_ERROR_UNKNOWN;
	return NULL_EVENT;
    
    
}


BOOLEAN ESP_restore(void){
    strcpy(at.command,"AT+RESTORE");
	strcpy(at.response,"OK");

	if (!sendCheckReply(at.command,at.response,DEFAULT_TIMEOUT_MS)){
		return false;
  	}
	return true;
}

BOOLEAN ESP_sleep(int16 time){
	sprintf(at.command,"AT+GSLP=%ld",time);
	strcpy(at.response,"OK");

	if (!sendCheckReply(at.command,at.response,DEFAULT_TIMEOUT_MS)){
		return false;
  	}
	return true;
}

void ESP_version(void){
	strcpy(at.command,"AT+GMR");
    strcpy(at.response,"OK");
    at_config.multiline = true; //se habilita porque envia un chingo de info con salto delinea
	if (getReply(at.command,DEFAULT_TIMEOUT_MS) > 1) //Si si llego algo...
	{
		fprintf(pc,"%s",at.replybuffer);
	}else{
		fprintf(pc,"Version error\r\n");
	}
    at_config.multiline = false;
	
}

boolean ESP_init(boolean hw_reset){
    
    output_high(ESP_ENABLE);
    if(hw_reset){
        radio_reset();
        delay_ms(2000); 
    }
  	strcpy(at.command, "AT");
  	strcpy(at.response,"OK");
	sendCheckReply(at.command,at.response,DEFAULT_TIMEOUT_MS);
	delay_ms(200);
	strcpy(at.command, "ATE0"); // apagamos ECHO
	sendCheckReply(at.command,at.response,DEFAULT_TIMEOUT_MS);
	delay_ms(200);
	strcpy(at.command, "AT");
	if (!sendCheckReply(at.command,at.response,DEFAULT_TIMEOUT_MS)){
		return false;
	}

  	return true;
  
}
/*************************************************************** */


/**************************SOFTAP FUNCTIONS */
BOOLEAN softAP_set_config(char *ssid, char *pwd, int channel, int encrypt){
    return true;

}
/***************************************** */