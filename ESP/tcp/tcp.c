
//#include "tcp.h"

/*************************TCP/IP functions */
int tcp_get_status(void){
  //at_config.multiline = true;
	int16 response;
	clean_buffer();
	strcpy(at.command,"AT+CIPSTATUS");
	strcpy(at.response,"STATUS:");
	if(sendParseReply(at.command,at.response,&response,' ',0,1)){
		//return true;
		//tcp.status = response;
        tcp.status = response;
        //at_config.multiline = false;
		return response;
	}
	//sendParseReplyChar(AT,RESP,'',0);
	tcp.status = 0;
    //at_config.multiline = false;
	return 0;

}


BOOLEAN tcp_start_connection(enum connection_type con_type,char *remote, int16 port){
	tcp_close_connection();
	clean_buffer();
    strcpy(at.command,"AT+CIPMUX=0");   // Disable multiple connections
	strcpy(at.response,"OK");          //Respuesta esperada
	if (!sendCheckReply(at.command,at.response,DEFAULT_TIMEOUT_MS)){
		return false;
  	}
    strcpy(at.command,"AT+CIPMODE=0");   // Normal transmission
	strcpy(at.response,"OK");          //Respuesta esperada
	if (!sendCheckReply(at.command,at.response,DEFAULT_TIMEOUT_MS)){
		return false;
  	}
	char type[4];
	if(con_type == TCP_TYPE){
		strcpy(type,"TCP");
	}else if(con_type == UDP_TYPE){
		strcpy(type,"UDP");
	}else if(con_type == SSL_TYPE){
		strcpy(type,"SSL");
	}else{
		return FALSE;
	}
	sprintf(at.command,"AT+CIPSTART=\"%s\",\"%s\",%ld",type,remote,port);
	strcpy(at.response,"CONNECT");
	//at_config.multiline = true;
	getReply(at.command,30000,1);
	//at_config.multiline = false;
	if(string_contains(at.response,at.replybuffer)){
		strcpy(at.response,"OK");
		if(string_contains(at.response,at.replybuffer)){
			return true;
		}
	}
	return false;
}

BOOLEAN tcp_close_connection(void){
	clean_buffer();
    strcpy(at.command,"AT+CIPCLOSE");   // Close single connection
	strcpy(at.response,"OK");          //Respuesta esperada
	if (!sendCheckReply(at.command,at.response,DEFAULT_TIMEOUT_MS)){
		return false;
  	}
	return true;
}

BOOLEAN tcp_send_data(char *data, int16 len){

	clean_buffer();
	sprintf(at.command,"AT+CIPSEND=%ld",len);   //Send "len" bytes
	strcpy(at.response,"> ");          //Respuesta esperada trae un espacio..
	if (!sendCheckReply(at.command,at.response,DEFAULT_TIMEOUT_MS)){
		return false;
  	}
	at_config.multiline = true;
    at_config.end_line = true;
	send_raw_data(data,len); //enviamos la data en raw
	if(timeout_data(5000)){ // esperamos que nos conteste...o timeout
		at.data_available = FALSE;
    	at_config.multiline =false;
		return false;
	}
    sprintf(at.response,"Recv %ld bytes",len);
	//delay_ms(500);
	//strcpy(at.response,"SEND OK");          //Respuesta esperada
    
	if (!string_contains(at.response,at.replybuffer))
	{
		fprintf(pc, "string no contains\r\n");
		return false;
	}
    at.data_available = FALSE;
    timeout_data(150);// esperamos que nos regrese el send ok
    //delay_ms(150); // parche para recibir ambos comandos
    strcpy(at.response,"SEND OK");          //Respuesta esperada
    
	if (!string_contains(at.response,at.replybuffer))
	{
		fprintf(pc, "string no contains\r\n");
		return false;
	}
    at_config.multiline =false;
    at.data_available = false;
	return true;
}


/**************************************** */
