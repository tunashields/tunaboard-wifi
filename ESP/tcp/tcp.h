#ifndef _tcp_H
#define _tcp_H
#define TCP_BUFFER_SIZE 500
enum tcp_connection_status{
    UNKNOWN_STATUS = 0,
    ESP_CONNECTED_TO_AP = 2,
    ESP_HAS_CREATED_TRANSMISSION = 3,
    TCP_TRANSMISSION_DISCONNECTED = 4,
    ESP_DOES_NOT_CONNECTED = 5,
};
enum connection_type{
    TCP_TYPE, // for tcp connection
    UDP_TYPE, // for udp connection
    SSL_TYPE // for ssl connection
};

enum tcp_responses{
    NULL_RESPONSE,
    RECEIVE_NETWORK_DATA,
    CONNECT,
    CLOSED
};

struct tcp_information
{
    enum connection_type connection;
    enum tcp_responses response;
    enum tcp_connection_status status;
    char rcv_data[TCP_BUFFER_SIZE];
    int16 rcv_data_len;
    int data_available;
}tcp;



/*
 * Get the connection Status
 */
int tcp_get_status(void);

/*
*  Establece una conexion TCP
*/
BOOLEAN tcp_start_connection(enum connection_type con_type,char *remote, int16 port);


/*
close TCP connection, single connection
 */
BOOLEAN tcp_close_connection(void);
/*
* Send data in normal mode and single connection..
* data: data array
* len: data length
*/

BOOLEAN tcp_send_data(char *data, int16 len);
#include "tcp.c"


#endif
