
#include "http.h"



boolean HTTP_PARSE_DATA(char *datapost, char *data_received, int16 *status_code ){
    int16 datalen;
    char resp_header[10];
    strcpy(resp_header,"HTTP/1.1");
    char *p = strstr(data_received,resp_header);
    if (p == 0)return false;
    char *n = p;
    strcpy(resp_header,"\n\n");
    p = strstr(data_received,resp_header);
    if (p == 0)return false;
    //p+=stringLEN(p)-datalen; // se cambio STRLEN porque solo puede contar has 255
    int i;
    datalen = stringLEN(p);
    for (i = 0; i < datalen; i++) {
        datapost[i] = p[i];
        if(p=='\0')break;
    }
    datapost[i] = '\0';
    n += stringLEN(resp_header);
    
    n = strchr(n,' ');
    n++;
    

    
    *status_code = atol(n);
        
    return true;
}

boolean HTTP_POST(char *response_,int16 *statuscode){
    http.CONTENT_LENGTH = stringLEN(http.DATA);


  #ifdef _ESP_H
  if(!tcp_start_connection(TCP_TYPE,http.HOST,80)){
		#if HTTP_DEBUG == 1
		fprintf(pc,"[HTTP_DEBUG] Error en conexion\r\n");
		#endif
		return false;
	}
	#if HTTP_DEBUG == 1
	fprintf(pc,"[HTTP_DEBUG] Conectado a: %s\r\n",http.HOST);
	#endif
  #endif
  #ifdef _lantronix_h
  if (!CONNECT_SERVER(http.HOST,5000)) {
    return false;
  }
  #endif
  sprintf(raw_post,"POST %s HTTP/1.1\r\n%s\r\nContent-Type:%s\r\nAccept:*/*\r\nHost:%s\r\ncontent-length: %ld\r\nConnection:close\r\n\r\n%s",
  http.PATH,http.HEADERS,http.CONTENT_TYPE,http.HOST,http.CONTENT_LENGTH,http.DATA);
  #ifdef HTTP_DEBUG
    fprintf(pc, "[HTTP_DEBUG] RAW_POST: %s\r\n",raw_post );
  #endif
  #ifdef _ESP_H
    
    if(!tcp_send_data(&raw_post,stringLEN(raw_post))){
        #if HTTP_DEBUG == 1
        fprintf(pc,"[HTTP_DEBUG] Error en envio!\r\n");
        #endif

        return false;

        //fprintf(pc,"[MAIN] Send data SUCCESS!\r\n");
    }

    at_config.multiline = true; // esperamos un chingo de datos con CR
    //event_type esp_http_event;
    //esp32_http_event = ESP32_event_available(CHAR_TYPE);
    esp_http_event = NULL_EVENT;
    while (esp_http_event == NULL_EVENT){
        esp_http_event = ESP_event_available(CHAR_TYPE);// importante para recibir bytes
    }
    at_config.multiline = false;
    if (esp_http_event != TCP_EVENT){
        return false;
    }
    if (tcp.response != RECEIVE_NETWORK_DATA)
    {
        return false;
    }
    delay_ms(100);

    if (!HTTP_PARSE_DATA(response_,&tcp.rcv_data,statuscode)) {
        return false;

    }


#endif
//strcpy(mqtt_rcv_data,tcp.rcv_data);
//bufferncpy(mqtt_rcv_data,tcp.rcv_data,tcp.rcv_data_len);
	

  #ifdef _lantronix_h
  delay_ms(1000);
  getReply(&raw_post,5000);
  int16 cont_leng;
  strcpy(CMD,"Content-Length: ");

  parseReply( CMD, &cont_leng, ' ', 0);
  strcpy(CMD,"HTTP/1.1 ");

  parseReply( CMD, statuscode, ' ', 0);

  //fprintf(pc, "RESP CONTENT_LENGTH : %ld\r\n",cont_leng );
  #endif

  /*if(!HTTP_PARSE_DATA(cont_leng,response_)){
    return false;
  }*/
  #ifdef _lantronix_h

  strcpy(RESP, "D"); //espero la "D" que manda lantronix al desconectar
  if(!expectReply(RESP,10000)){
    fprintf(pc, "fallo respuesta de la D\r\n" );
    return false;
  }
  #endif

  //fprintf(pc, "RES BUFFER%s\r\n",res_buffer );
  tcp_close_connection();
  delay_ms(200);

  memset(http.DATA,0,sizeof(http.DATA));
  memset(tcp.rcv_data,0,sizeof(tcp.rcv_data));
  tcp.rcv_data_len = 0;
  
  return true;
}
