


/**
 * Instantiate a collection.
 * @arg n  Number of values in this collection.
 * @return A pointer to a collection.
 */
UbidotsCollection* ubidots_collection_init(int n) {
  UbidotsCollection *coll = malloc(sizeof(UbidotsCollection));

  coll->n = n;
  coll->i = 0;
  coll->variable_ids = malloc(sizeof(char*) * n);
  coll->values = malloc(sizeof(float) * n);

  return coll;
}


/**
 * Add a value to a collection.
 * @arg coll         Pointer to the collection made by ubidots_collection_init().
 * @arg variable_id  The ID of the variable this value is associated with.
 * @arg value        The value.
 */
void ubidots_collection_add(UbidotsCollection *coll, char *variable_id, double value) {
  int i = coll->i;

  int len = sizeof(char) * strlen(variable_id);
  coll->variable_ids[0][i] = malloc(len + 1);
  strcpy(coll->variable_ids[0][i], variable_id);
  
  fprintf(pc,"add[%i]: %s\r\n",i,coll->variable_ids[0][i]);

  coll->values[i] = value;
  
  fprintf(pc,"value[%i]: %f\r\n",i,coll->values[i]);

  coll->i++;
}


/**
 * Save a collection.
 * @arg coll Collection to save, pointer to save json data.
 * @return len of json data.
 */
int ubidots_collection_save(UbidotsCollection *coll, char *json_data) {
 
    char value[20];
    char json_complemento[20];
    // Encode JSON Payload
    int i, n = coll->n;
    sprintf(json_data,"{");
    for (i = 0; i < n; i++) {
        //fprintf(pc, "variable[%i]%s\r\n",i,coll->variable_ids[i]);
        //fprintf(pc, "value[%i]%f\r\n",i,coll->values[i]);

        strcat(json_data,coll->variable_ids[i]);
        strcpy(json_complemento,":{\"value\":");
        strcat(json_data,json_complemento);
        sprintf(value,"%f",coll->values[i]);      
        strcat(json_data,value);
        if ((i+1) < n )
        {
            strcpy(json_complemento,"},");
            strcat(json_data,json_complemento);
        }   
    }
    strcpy(json_complemento,"}}");
    strcat(json_data,json_complemento);

    return strlen(json_data);
}


/**
 * Cleanup a collection when after it is no longer being used.
 * @arg coll Pointer to the collection made by ubidots_collection_init().
 */
void ubidots_collection_cleanup(UbidotsCollection *coll) {
  int i, n = coll->n;

  for (i = 0; i < n; i++) {
    free(coll->variable_ids[i]);
  }

  free(coll->variable_ids);
  free(coll->values);
  free(coll);
}


/**
 * Save a value to Ubidots.
 * @arg client       Pointer to UbidotsClient
 * @arg variable_id  The ID of the variable to save to
 * @arg value        The value to save
 * @arg timestamp    Timestamp (millesconds since epoch). Pass TIMESTAMP_NOW
 *                   to have the timestamp automatically calculated.
 * @return Zero upon success, Non-zero upon error.
 */
int ubidots_save_value(char *variable_id, double value) {
  /* char url[80];
  char json_data[80];

  if (timestamp == TIMESTAMP_NOW)
    timestamp = (long long)time(NULL) * 1000;

  sprintf(url, "%s/variables/%s/values", client->base_url, variable_id);
  sprintf(json_data, "{\"value\": %g, \"timestamp\": %lld}", value, timestamp);

  return ubi_request("POST", url, client->token, json_data, NULL);*/
    return 1;
}


/**
 * Initialize a Ubidots session. This is most likely the first Ubidots
 * library function you will call.
 * @arg Token  Your token key for the Ubidots hw client.
 * @return Upon success, a pointer to a UbidotsClient. Upon error, NULL.
 */
/*UbidotsClient* ubidots_init(char *token) {
  return ubidots_init_with_base_url(token, DEFAULT_BASE_URL);
}


UbidotsClient* ubidots_init_with_base_url(char *api_token, char *base_url) {
 
  // Allocate and set fields of struct
  UbidotsClient *client = malloc(sizeof(UbidotsClient));  

  strncpy(client->base_url, base_url, STRLEN_BASE_URL);
  strncpy(client->token, token, STRLEN_TOKEN);

  return client;
}
*/

/**
 * End a ubidots session. After calling this function with UbidotsClient* client,
 * no more functions may be called with it.
 */
/*void ubidots_cleanup(UbidotsClient *client) {
  //free(client);
}*/