#ifndef _ubidots_H
#define _ubidots_H

#define DEFAULT_BASE_URL "http://things.ubidots.com/api/v1.6"
#define STRLEN_BASE_URL 100
#define STRLEN_TOKEN    100

#define TIMESTAMP_NOW -1
/*
typedef struct UbidotsClient
{
    char base_url[STRLEN_BASE_URL];
    char token [STRLEN_TOKEN];
}UbidotsClient;
*/

 typedef struct UbidotsCollection {
  int n;
  int i;
  char  **variable_ids;
  float *values;
} UbidotsCollection;


//UbidotsClient* ubidots_init(char *token);
//UbidotsClient* ubidots_init_with_base_url(char *token, char *base_url);
//int ubidots_save_value(UbidotsClient *client, char *variable_id, double value, long long timestamp);
int ubidots_save_value(char *variable_id, double value);
//void ubidots_cleanup(UbidotsClient *client);

UbidotsCollection* ubidots_collection_init(int n);
void ubidots_collection_add(UbidotsCollection *coll, char *variable_id, double value);
int ubidots_collection_save(UbidotsCollection *coll, char *json_data) ;
void ubidots_collection_cleanup(UbidotsCollection *coll);


#include "ubidots.c"
#endif