#ifndef _TUNAI2C_H
#define _TUNAI2C_H

#ifndef I2C_SDA

#define I2C_SDA  PIN_C4
#define I2C_SCL  PIN_C3

#endif
#define I2C_DEBUG 1

#use i2c(master, sda=I2C_SDA, scl=I2C_SCL,SLOW,FORCE_HW)
#include "stdint.h"



/*=================================================================================
                Funcion para escribir bytes en direcciones de memoria
*/
void TunaI2C_WriteAddress(uint8_t i2cAddr, uint8_t memAddr, uint8_t *data, uint16_t dataSize);
/*=================================================================================
                Funcion para leer bytes de direcciones de memoria
*/
void TunaI2C_ReadAddress(uint8_t i2cAddr, uint8_t memAddr, uint8_t *data, uint16_t dataSize);
/*=================================================================================
                Funcion para escribir 1 bytes en 1 direccion de memoria
*/
void Tuna_I2C_WriteByteAddress(uint8_t i2cAddr, uint8_t memAddr, uint8_t data);
/*=================================================================================
                Funcion para leer 1 bytes de 1 direccion de memoria
*/
void TunaI2C_ReadByteAddress(uint8_t i2cAddr, uint8_t memAddr, uint8_t *data);

/*=================================================================================
                Funcion para revisar si el dispositivo i2c slave esta disponible
*/
boolean TunaI2C_ready(uint8_t i2cAddr);

/*=================================================================================
                Funcion para iniciar lineas de transmisicion
*/
void TunaI2C_init(void);

#endif // _GI2C_H
