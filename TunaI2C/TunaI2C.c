#include "TunaI2C.h"
#include <stdint.h>
void TunaI2C_WriteAddress(uint8_t i2cAddr, uint8_t memAddr, uint8_t *data, uint16_t dataSize){
    uint8_t w,r;
    w = i2cAddr & 0xFE;
    r = i2cAddr;
    
    i2c_start();
    //delay_ms(50);
    i2c_write(w);
    uint8_t i;
    i2c_write(memAddr);
    //delay_ms(50);
    //for(i=0;i<memAddrSize;i++){
        //i2c_write(memAddr[i]);
        //delay_ms(50);
    //}
    for(i=0;i<dataSize;i++){
        i2c_write(data[i]);
        //delay_ms(50);
    }
    i2c_stop();
}

void TunaI2C_ReadAddress(uint8_t i2cAddr, uint8_t memAddr, uint8_t *data, uint16_t dataSize){
    uint8_t w,r;
    w = i2cAddr & 0xFE;
    r = i2cAddr;
  
    #if I2C_DEBUG == 1
    fprintf(pc, "[I2C_DEBUG] WriteAddr %X\r\n",w);
    fprintf(pc, "[I2C_DEBUG] ReadAddr %X\r\n",r);
    #endif
    i2c_start();
    //delay_ms(50);
    i2c_write(w);
    i2c_write(memAddr);
    i2c_stop();
    uint8_t i;
    //delay_ms(50);
    i2c_start();
    i2c_write(r);
    for(i=0;i<dataSize - 1;i++){
        data[i] = i2c_read();
        //delay_ms(50);
    }
    data[dataSize - 1] = i2c_read(0);          //Nack last read
    //delay_ms(50);
    i2c_stop();
    delay_ms(100);

    #if I2C_DEBUG == 1
    fprintf(pc, "[I2C_DEBUG] Recibido: ");
    for (int d = 0; d < dataSize; d++) {
      fprintf(pc, "%X ",data[d] );
    }
    fprintf(pc, "\r\n" );
    #endif
}

void TunaI2C_ReadByteAddress(uint8_t i2cAddr, uint8_t memAddr, uint8_t *data){
    //GI2C_ReadAddress(i2cAddr,&memAddr,1,data,1);
}

void TunaI2C_WriteByteAddress(uint8_t i2cAddr, uint8_t memAddr, uint8_t data){
    //TunaI2C_WriteAddress(i2cAddr,&memAddr,1,&data,1);
}

boolean TunaI2C_ready(uint8_t i2cAddr){
    uint8_t w;
    w = i2cAddr & 0xFE;
   int1 ack;
   i2c_start();
   ack = i2c_write(w);
   i2c_stop();
   return !ack;
}

void TunaI2C_init(void)
{
   output_float(I2C_SCL);
   output_float(I2C_SDA);

}
