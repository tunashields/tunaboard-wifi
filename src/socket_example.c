/*
 * Project: TunaBoard-Wifi SOCKET ESP32
 * Description: Ejemplo de SOCKET ESP32
 * Author: Arturo Gasca
 * Date:  Julio-2019
 * Development: Tuna Shields
 *
 */
#include <18F46K22.h>
#use delay(crystal=20mhz)
#use rs232(baud=115200,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8,stream=pc,restart_wdt)

#define LED1 PIN_B0
#define LED2 PIN_B1

#include <string.h>
#include <stdlib.h>
#include <stdlibm.h>
#include "../ESP32/ESP32.h"
//#include "mqtt/mqtt.c" //<--modificar pines de serial y reset y macro INT_RDA
//#include "internet-time/internet-time.c"

//char ssid[] = {"INFINITUM1155"};
//char pass[] = {"37YHXC3nzs"};
char ssid[] = {"vialactea"};
char pass[] = {"bobyestaenmarte"};
//char ssid[] = {"HUAWEI P smart"};
//char pass[] = {"arturo123456"};
char data[100];



char server[] = {"192.168.1.73"};
int16 port = 10000;

#define POLLING_TIME 20
int segundos = 0;
int cuenta = 0;
boolean SEND_DATA =true;

event_type esp32_event;

#INT_TIMER0
void  TIMER0_isr(void){
 cuenta++;
 if(cuenta >=5){
	 cuenta =0;
	 segundos++;
		if (segundos >=POLLING_TIME) {
			segundos = 0;
			SEND_DATA = true;
		}
 }
}


#INT_RDA2
void radio_isr(){
	radio_receive();
}


void check_ESP32_event(int event){
	fprintf(pc,"[EVENT]");
	switch (event)
	{
	case WIFI_CONNECTED:
		fprintf(pc,"WIFI_CONNECTED\r\n");
		break;
	case WIFI_GOT_IP:
		fprintf(pc,"WIFI_GOT_IP\r\n");
		break;
	case WIFI_DISCONNECTED:
		fprintf(pc,"WIFI_DISCONNECTED\r\n");
		break;
	case WIFI_CONNECTION_TIMEOUT:
		fprintf(pc,"WIFI_CONNECTION_TIMEOUT\r\n");
		break;
	case WIFI_WRONG_PASSWORD:
		fprintf(pc,"WIFI_WRONG_PASSWORD\r\n");
		break;
	case WIFI_CANNOT_FIND_AP:
		fprintf(pc,"WIFI_CANNOT_FIND_AP\r\n");
		break;
	case WIFI_CONNECTION_FILED:
		fprintf(pc,"WIFI_CONNECTION_FILED\r\n");
		break;

	default:
		fprintf(pc,"UNKNOWN\r\n");
		break;
	}

}

void check_tcp_status(tcp_connection_status res){
	fprintf(pc,"[STATUS] ");
	switch (res)
	{
	case ESP_CONNECTED_TO_AP:
		fprintf(pc,"ESP_CONNECTED_TO_AP\r\n");
		break;
	case ESP_HAS_CREATED_TRANSMISSION:
		fprintf(pc,"ESP_HAS_CREATED_TRANSMISSION\r\n");
		break;

	case TCP_TRANSMISSION_DISCONNECTED:
		fprintf(pc,"TCP_TRANSMISSION_DISCONNECTED\r\n");
		break;
	case ESP_DOES_NOT_CONNECTED:
		fprintf(pc,"ESP_DOES_NOT_CONNECTED\r\n");
		break;
	default:
		fprintf(pc,"UNKNOWN_STATUS TCP\r\n");
		break;
	}
}
void main(void){

	set_tris_a(0b00000000);
	set_tris_b(0b00000001);
	set_tris_c(0b10000000);
	set_tris_d(0b10000000);
	set_tris_e(0b000);
	setup_timer_0(RTCC_INTERNAL|RTCC_DIV_16);		//210 ms overflow
	enable_interrupts(GLOBAL);
	enable_interrupts(INT_TIMER0);
	enable_interrupts(INT_RDA2);
	output_low(LED1);
	delay_ms(1000);

	if (esp32_init())
	{
		fprintf(pc,"Modulo OK\r\n");
	}else{
		fprintf(pc,"modulo no responde\r\n");
	}
    wifi_mode(WIFI_STATION);
    wifi_auto_connect(FALSE);
    if(wifi_connect(ssid,pass)){
		fprintf(pc,"[MAIN] Conectado!!..\r\n");
		fprintf(pc,"IP address: %s\r\n",wifi.ip);
	}else{
        fprintf(pc,"[MAIN] Error en conexion!!..\r\n");
        //check_wifi_event(wifi.status);
	}

	wifi_get_info(wifi.ip,wifi.gateway,wifi.netmask);
	fprintf(pc,"[MAIN] IP: %s\r\n",wifi.ip);
	fprintf(pc,"[MAIN] GW: %s\r\n",wifi.gateway);
	fprintf(pc,"[MAIN] NM: %s\r\n",wifi.netmask);
	tcp_get_status();
	check_tcp_status(tcp.status);



	if(tcp_start_connection(TCP_TYPE,server,port)){
		fprintf(pc,"[MAIN] Conectado a: %s:%ld\r\n",server,port);
	}else{
		fprintf(pc,"[MAIN] Error en la conexion tcp\r\n");
	}
	sprintf(data,"Hola mundo!");
	if(tcp_send_data(data,strlen(data))){
		fprintf(pc,"[MAIN] Send data SUCCESS!\r\n");
	}
	else{
		fprintf(pc,"[ERROR] Send Fail\r\n");
	}



	at_config.end_line = false;
	while(true){
        esp32_event = ESP32_event_available(BYTE_TYPE);
        if(esp32_event != NULL_EVENT){
            //check_ESP32_event(wifi.status);
            fprintf(pc,"Recibi un evento!!\r\n");
            if(esp32_event == TCP_EVENT){
                fprintf(pc,"evento TCP!!\r\n");
                if (tcp.data_available) {
                    fprintf(pc,"[MAIN] TCP Data available\r\n");
                    fprintf(pc,"[MAIN] Data [ ");
                    int i;
                    for (i = 0; i < tcp.rcv_data_len; i++) {
                        fprintf(pc,"%X",tcp.rcv_data[i]);

                    }
                    fprintf(pc," ]\r\n");


                }

            }
            else if(esp32_event == WIFI_EVENT){
                fprintf(pc,"evento WIFI!!\r\n");
            }

        }

	}
}
