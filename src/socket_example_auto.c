/*
 * Project: TunaBoard-Wifi SOCKET AUTO ESP32
 * Description: Ejemplo para usar conexion automatica de wifi y socket
 * Author: Arturo Gasca
 * Date:  Julio-2019
 * Development: Tuna Shields
 * 
 * Este ejemplo se usa cuando el ESP ya tiene una configuracion WiFi almacenada
 * con la funcion wifi_auto_connect(TRUE);
 * Se debe considerar un hilo extra para realizar esta configuracion
 * Para fines practicos, descomentar el proceso de wifi para realizar una
 * primera conexion, posterior a eso, comentar el proceso de wifi para
 * seguir con la logica del programa
 * 
 * Se utiliza una maquina de estados para saber cuando hacer conexi�n
 * 
 *importante siempre utilizar ESP32_event_available(); en el loop principal
 * esta funcion se encarga de actualizar los eventos recibidos por el serial
 * A pesar de tener el puerto UART por interrupcion, es necesario llamar esta 
 * funcion para analizar el buffer de entrada
 * 
 *
 */
#include <18F46K22.h>
#use delay(crystal=20mhz)
#use rs232(baud=115200,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8,stream=pc,restart_wdt)

#define LED1 PIN_B0
#define LED2 PIN_B1

#include <string.h>
#include <stdlib.h>
#include <stdlibm.h>
#include "../ESP32/ESP32.h"
//#include "mqtt/mqtt.c" //<--modificar pines de serial y reset y macro INT_RDA
//#include "internet-time/internet-time.c"

//char ssid[] = {"INFINITUM1155"};
//char pass[] = {"37YHXC3nzs"};
char ssid[] = {"vialactea"};
char pass[] = {"bobyestaenmarte"};
//char ssid[] = {"HUAWEI P smart"};
//char pass[] = {"arturo123456"};
char data[100];



char server[] = {"192.168.1.73"};
int16 port = 10000;

#define POLLING_TIME 20
int segundos = 0;
int cuenta = 0;
boolean SEND_DATA =true;

event_type esp32_event;

#INT_TIMER0
void  TIMER0_isr(void){
 cuenta++;
 if(cuenta >=5){
	 cuenta =0;
	 segundos++;
		if (segundos >=POLLING_TIME) {
			segundos = 0;
			SEND_DATA = true;
		}
 }
}


#INT_RDA2
void radio_isr(){
	radio_receive();
}
boolean wifi_ready = false;
boolean server_connected = false;

void check_ESP32_event(enum event_type incomming_event){
    
	switch(incomming_event){
        case WIFI_EVENT:
            fprintf(pc,"[WIFI_EVENT]\r\n");
            fprintf(pc," >>");
            switch (wifi.status){
                
                case WIFI_CONNECTED:
                    fprintf(pc,"WIFI_CONNECTED\r\n");
                    break;
                case WIFI_GOT_IP:
                    fprintf(pc,"WIFI_GOT_IP\r\n");
                    ESP32_init(FALSE);//sin hw_reset
                    wifi_ready = true;
                    break;
                case WIFI_DISCONNECTED:
                    fprintf(pc,"WIFI_DISCONNECTED\r\n");
                    break;
                case WIFI_CONNECTION_TIMEOUT:
                    fprintf(pc,"WIFI_CONNECTION_TIMEOUT\r\n");
                    break;
                case WIFI_WRONG_PASSWORD:
                    fprintf(pc,"WIFI_WRONG_PASSWORD\r\n");
                    break;
                case WIFI_CANNOT_FIND_AP:
                    fprintf(pc,"WIFI_CANNOT_FIND_AP\r\n");
                    break;
                case WIFI_CONNECTION_FILED:
                    fprintf(pc,"WIFI_CONNECTION_FILED\r\n");
                    break;
                default:
                    fprintf(pc,"UNKNOWN\r\n");
                    break;
            }
            
            break;
        case TCP_EVENT:
            fprintf(pc,"[TCP_EVENT]\r\n"); 
            fprintf(pc," >>");
            switch(tcp.response){
                case RECEIVE_NETWORK_DATA:
                    fprintf(pc,"[RECEIVE_NETWORK_DATA]\r\n");                               
                    fprintf(pc, " >>Data available %d\r\n",tcp.data_available);
                    fprintf(pc, " >>Data length %ld\r\n",tcp.rcv_data_len);
                    if(at_config.end_line){  //define si recibo bytes o strings
                        fprintf(pc," >>Data: [%s]\r\n",tcp.rcv_data);
                    }else{
                        fprintf(pc," >>Data: [");
                        int i;
                        for (i = 0; i < tcp.rcv_data_len; i++) {
                            fprintf(pc,"%X",tcp.rcv_data[i]);
                        }
                        fprintf(pc,"]\r\n");
                    }
                    break;
                case CONNECT:
                    fprintf(pc,"[CONNECT]\r\n");  
                    server_connected = true;
                    break;
                case CLOSED:
                    fprintf(pc,"[CLOSED]\r\n");  
                    break;
            }                                            
            break;
        case ESP_EVENT:
            fprintf(pc,"[ESP_EVENT]\r\n");
            break;
        default:
            fprintf(pc,"[UNKNOWN_EVENT ??]\r\n");
            break;
         
    }
}

void check_tcp_status(tcp_connection_status res){
	fprintf(pc,"[STATUS] ");
	switch (res)
	{
	case ESP_CONNECTED_TO_AP:
		fprintf(pc,"ESP_CONNECTED_TO_AP\r\n");
		break;
	case ESP_HAS_CREATED_TRANSMISSION:
		fprintf(pc,"ESP_HAS_CREATED_TRANSMISSION\r\n");
		break;

	case TCP_TRANSMISSION_DISCONNECTED:
		fprintf(pc,"TCP_TRANSMISSION_DISCONNECTED\r\n");
		break;
	case ESP_DOES_NOT_CONNECTED:
		fprintf(pc,"ESP_DOES_NOT_CONNECTED\r\n");
		break;
	default:
		fprintf(pc,"UNKNOWN_STATUS TCP\r\n");
		break;
	}
}
void wifi_process(void);
void tcp_connection_process(void);
void main(void){

	set_tris_a(0b00000000);
	set_tris_b(0b00000001);
	set_tris_c(0b10000000);
	set_tris_d(0b10000000);
	set_tris_e(0b000);
	setup_timer_0(RTCC_INTERNAL|RTCC_DIV_16);		//210 ms overflow
	enable_interrupts(GLOBAL);
	enable_interrupts(INT_TIMER0);
	enable_interrupts(INT_RDA2);
	output_low(LED1);
	delay_ms(1000);
    fprintf(pc,"ESP32 Socket auto wifi example\r\n");
    radio_reset();
    
    //wifi_process();
    
	at_config.end_line = true; //Se pone en false cuando vamos a estar recibiendo hexadecimales desde TCP
	while(true){
        //esp32_event = ESP32_event_available(BYTE_TYPE);
        esp32_event = ESP32_event_available();
        if(esp32_event != NULL_EVENT){
           check_ESP32_event(esp32_event);
        }
        if(wifi_ready){
            delay_ms(2000);
            wifi_ready= false;
            if(tcp_start_connection(TCP_TYPE,server,port)){
                fprintf(pc,"[MAIN] Conectado a: %s:%ld\r\n",server,port);
                server_connected = true;
            }else{
                fprintf(pc,"[MAIN] Error en la conexion tcp\r\n");
            }                                    
        }
        if(server_connected){
            delay_ms(2000);
            server_connected = false;
            sprintf(data,"Hola mundo!");
            if(tcp_send_data(data,strlen(data))){
                fprintf(pc,"[MAIN] Send data SUCCESS!\r\n");
            }
            else{
                fprintf(pc,"[ERROR] Send Fail\r\n");
            }
        }

	}
}



void wifi_process(void){
    if (esp32_init())
	{
		fprintf(pc,"Modulo OK\r\n");
	}else{
		fprintf(pc,"modulo no responde\r\n");
	}
  
    wifi_mode(WIFI_STATION);
    wifi_auto_connect(TRUE); // cambiar dependiendo el modo de conexion deseado, previo a un reset
    if(wifi_connect(ssid,pass)){
		fprintf(pc,"[MAIN] Conectado!!..\r\n");
		fprintf(pc,"IP address: %s\r\n",wifi.ip);
	}else{
        fprintf(pc,"[ERROR] Error en conexion!!..\r\n");
        check_ESP32_event(wifi.status);
        fprintf(pc,"[ERROR] Please reset\r\n");
        while(true);
	}

	wifi_get_info(wifi.ip,wifi.gateway,wifi.netmask);
	fprintf(pc,"[MAIN] IP: %s\r\n",wifi.ip);
	fprintf(pc,"[MAIN] GW: %s\r\n",wifi.gateway);
	fprintf(pc,"[MAIN] NM: %s\r\n",wifi.netmask);
	tcp_get_status();
	check_tcp_status(tcp.status);
	
}

void tcp_connection_process(void){
    if(tcp_start_connection(TCP_TYPE,server,port)){
		fprintf(pc,"[MAIN] Conectado a: %s:%ld\r\n",server,port);
	}else{
		fprintf(pc,"[MAIN] Error en la conexion tcp\r\n");
	}
}