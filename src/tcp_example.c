#include <tunaboard/tunaone.h>
#use rs232(baud=115200,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8,stream=pc,restart_wdt)
#define RADIO_RST   PIN_D1
#define RADIO_RX    PIN_D7
#define RADIO_TX    PIN_D6
#define RADIO_BAUD  115200
#define DEBUG       0
#define RADIO_SERIAL_SOURCE RADIO_INT_RDA2

#define ESP_ENABLE PIN_D0

#include <string.h>
#include <stdlib.h>
#include <stdlibm.h>
#include "../AT/AT.h"
#include "../ESP/ESP.h"

#define DHT_PIN     PIN_A0
#define DHT_TYPE    DHT22
long t,h;
#include <tuna-lib/DHT.c>

char ssid[] = {"INFINITUM9159_2.4"};
char pass[] = {"4PwyP2nkRJ"};

char server[] = {"192.168.1.71"};
int16 port = 10000;
char mac [] = {"00204AEA8B3E"}; //http
char datos[50];


#define POLLING_TIME 30
int16 segundos = 0;
int16 cuenta = 0;


BOOLEAN SEND_DATA =true;

event_type esp8266_event;

int16 nbitsADC;
float temperature;

#INT_TIMER0
void  TIMER0_isr(void){
 cuenta++;
 if(cuenta >=5){
     cuenta =0;
	 segundos++;
     
     if (segundos >=POLLING_TIME) {
         segundos = 0;
         SEND_DATA = true;
     }

   }
}

boolean wifi_ready = false;
boolean server_connected = false;

void check_ESP32_event(enum event_type incomming_event){
    
	switch(incomming_event){
        case WIFI_EVENT:
            fprintf(pc,"[WIFI_EVENT]\r\n");
            fprintf(pc," >>");
            switch (wifi.status){
                
                case WIFI_CONNECTED:
                    fprintf(pc,"WIFI_CONNECTED\r\n");
                    break;
                case WIFI_GOT_IP:
                    fprintf(pc,"WIFI_GOT_IP\r\n");
                    ESP_init(FALSE);//sin hw_reset
                    wifi_ready = true;
                    break;
                case WIFI_DISCONNECTED:
                    fprintf(pc,"WIFI_DISCONNECTED\r\n");
                    break;
                case WIFI_CONNECTION_TIMEOUT:
                    fprintf(pc,"WIFI_CONNECTION_TIMEOUT\r\n");
                    break;
                case WIFI_WRONG_PASSWORD:
                    fprintf(pc,"WIFI_WRONG_PASSWORD\r\n");
                    break;
                case WIFI_CANNOT_FIND_AP:
                    fprintf(pc,"WIFI_CANNOT_FIND_AP\r\n");
                    break;
                case WIFI_CONNECTION_FILED:
                    fprintf(pc,"WIFI_CONNECTION_FILED\r\n");
                    break;
                default:
                    fprintf(pc,"UNKNOWN\r\n");
                    break;
            }
            
            break;
        case TCP_EVENT:
            fprintf(pc,"[TCP_EVENT]\r\n"); 
            fprintf(pc," >>");
            switch(tcp.response){
                case RECEIVE_NETWORK_DATA:
                    fprintf(pc,"[RECEIVE_NETWORK_DATA]\r\n");                               
                    fprintf(pc, " >>Data available %d\r\n",tcp.data_available);
                    fprintf(pc, " >>Data length %ld\r\n",tcp.rcv_data_len);
                    if(at_config.end_line){  //define si recibo bytes o strings
                        fprintf(pc," >>Data: [%s]\r\n",tcp.rcv_data);
                    }else{
                        fprintf(pc," >>Data: [");
                        int i;
                        for (i = 0; i < tcp.rcv_data_len; i++) {
                            fprintf(pc,"%X",tcp.rcv_data[i]);
                        }
                        fprintf(pc,"]\r\n");
                    }
                    break;
                case CONNECT:
                    fprintf(pc,"[CONNECT]\r\n");  
                    server_connected = true;
                    break;
                case CLOSED:
                    fprintf(pc,"[CLOSED]\r\n");  
                    break;
            }                                            
            break;
        case ESP_EVENT:
            fprintf(pc,"[ESP_EVENT]\r\n");
            break;
        default:
            fprintf(pc,"[UNKNOWN_EVENT ??]\r\n");
            break;
         
    }
}

void check_tcp_status(tcp_connection_status res){
	fprintf(pc,"[STATUS] ");
	switch (res)
	{
	case ESP_CONNECTED_TO_AP:
		fprintf(pc,"ESP_CONNECTED_TO_AP\r\n");
		break;
	case ESP_HAS_CREATED_TRANSMISSION:
		fprintf(pc,"ESP_HAS_CREATED_TRANSMISSION\r\n");
		break;

	case TCP_TRANSMISSION_DISCONNECTED:
		fprintf(pc,"TCP_TRANSMISSION_DISCONNECTED\r\n");
		break;
	case ESP_DOES_NOT_CONNECTED:
		fprintf(pc,"ESP_DOES_NOT_CONNECTED\r\n");
		break;
	default:
		fprintf(pc,"UNKNOWN_STATUS TCP\r\n");
		break;
	}
}

void wifi_process(void);
float get_temperature(void);
void check_wifi_status(void);
void construyeJson(void);

void main(void){
    set_tris_a(0b00000001);
	set_tris_b(0b00000001);
	set_tris_c(0b10000000);
	set_tris_d(0b10000000);
	set_tris_e(0b000);
	setup_timer_0(RTCC_INTERNAL|RTCC_DIV_16);		//210 ms overflow
	
	enable_interrupts(INT_TIMER0);
	enable_interrupts(INT_RDA2);
    enable_interrupts(GLOBAL);
	output_low(LED1);
    DHT_init();
	delay_ms(1000);
    fprintf(pc,"ESP8266 TCP example\r\n");
    
    wifi_process();
    while(true){
        esp8266_event = ESP_event_available(CHAR_TYPE);
        if(esp8266_event != NULL_EVENT){
            check_ESP32_event(esp8266_event);
        }
        if(wifi.last_event == WIFI_GOT_IP){
            //check_wifi_status();
            
            
        }
        if(wifi.status == WIFI_GOT_IP){
            if (SEND_DATA)
            {
                tcp_get_status();
                delay_ms(100);
                fprintf(pc, "Estado tcp: %d\r\n",tcp.status);
                if (tcp.status != ESP_HAS_CREATED_TRANSMISSION) {                    
                    if(tcp_start_connection(TCP_TYPE,&server,port)){
                    fprintf(pc,"conexion tcp establecida \r\n");
                    
                    }
                    else{
                        fprintf(pc,"Fallo la conexion tcp\r\n");
                    }
                }
                SEND_DATA = FALSE;
				construyeJson();
                fprintf(pc,"Enviando datos...\r\n");
                tcp_send_data(&datos,strlen(datos));
            }            
        }
        
    }
}


void wifi_process(void){
    if (ESP_init(1))
	{
		fprintf(pc,"Modulo OK\r\n");
	}else{
		fprintf(pc,"modulo no responde\r\n");
        while(TRUE);
	}
  
    wifi_mode(WIFI_STATION);
    wifi_auto_connect(FALSe); // cambiar dependiendo el modo de conexion deseado, previo a un reset
    if(wifi_connect(ssid,pass)){
		fprintf(pc,"[MAIN] Conectado!!..\r\n");
		fprintf(pc,"IP address: %s\r\n",wifi.ip);
	}else{
        fprintf(pc,"[ERROR] Error en conexion!!..\r\n");
        check_ESP32_event(wifi.status);
        fprintf(pc,"[ERROR] Please reset\r\n");
        while(true);
	}
}

void check_wifi_status(void){
    wifi_get_info(wifi.ip,wifi.gateway,wifi.netmask);
	fprintf(pc,"[MAIN] IP: %s\r\n",wifi.ip);
	fprintf(pc,"[MAIN] GW: %s\r\n",wifi.gateway);
	fprintf(pc,"[MAIN] NM: %s\r\n",wifi.netmask);
	tcp_get_status();
	check_tcp_status(tcp.status);
}

void construyeJson(void){
     if(DHT_Get_Values(&t,&h) == DHT_OK){
        fprintf(pc, "H: %3.1w%% T: %3.1wC\r\n", h,t);        
    }
    else{
        fprintf(pc, "SENSOR FAIL!!\r\n");
    }
     //temperature = get_temperature();
     sprintf(datos,"{\"temp\":%3.1w,\"hum\":%3.1w}",t,h);
}