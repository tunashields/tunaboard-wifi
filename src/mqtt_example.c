/*
 * Project: TunaBoard-Wifi MQTTESP32
 * Description: Ejemplo para usar MQTT con ESP32
 * Author: Arturo Gasca
 * Date:  Julio-2019
 * Development: Tuna Shields
 * 

 * 
 *
 */
#include <18F46K22.h>
#device adc=10
#use delay(crystal=20mhz)
#use rs232(baud=115200,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8,stream=pc,restart_wdt)


#define LED1 PIN_B0
#define LED2 PIN_B1

#include <string.h>
#include <stdlib.h>
#include <stdlibm.h>
#include "../ESP32/ESP32.h"
#include "../mqtt/mqtt.h"
//#include "mqtt/mqtt.c" //<--modificar pines de serial y reset y macro INT_RDA
//#include "internet-time/internet-time.c"

//char ssid[] = {"INFINITUM1155"};
//char pass[] = {"37YHXC3nzs"};
//char ssid[] = {"vialactea"};
//char pass[] = {"bobyestaenmarte"};
char ssid[] = {"HUAWEI P smart"};
char pass[] = {"arturo123456"};
char data[100];



//char server[] = {"192.168.1.73"};
//int16 port = 10000;
//char broker[] = {"things.ubidots.com"};
char broker[] = {"industrial.api.ubidots.com"};
int16 port = 1883;
//char broker[] = {"74.208.169.198"};
//int16 port = 1884;

char mqtt_username[] = {"tunamqtt"};
char mqtt_pass[] = {"tun@sh13ldsiot"};
char mac [] = {"tunaboard_mqtt"};
char topicOut[50];
char topicIn[50] ;
char payload[50] ;

char ubidots_token[] = {"BBFF-eQoPAKHWVcFiKdU7hvVkL4HYvy5pQz"};
char ubidots_pass[] = {"tunashields"};

#define POLLING_TIME 20
int16 segundos = 0;
int16 cuenta = 0;
int16 keepalive_sec = 0;

BOOLEAN KEEPALIVE = false;
BOOLEAN SEND_DATA =true;

event_type esp32_event;

int16 nbitsADC;
float temperature;

#INT_TIMER0
void  TIMER0_isr(void){
 cuenta++;
 if(cuenta >=5){
     cuenta =0;
	 segundos++;
     keepalive_sec++;
     if (segundos >=POLLING_TIME) {
         segundos = 0;
         SEND_DATA = true;
     }
     if (keepalive_sec >= MQTT_CONN_KEEPALIVE) {
         keepalive_sec = 0;
         KEEPALIVE =true;

        }

   }
}


#INT_RDA2
void radio_isr(){
	radio_receive();
}
boolean wifi_ready = false;
boolean server_connected = false;

void check_ESP32_event(enum event_type incomming_event){
    
	switch(incomming_event){
        case WIFI_EVENT:
            fprintf(pc,"[WIFI_EVENT]\r\n");
            fprintf(pc," >>");
            switch (wifi.status){
                
                case WIFI_CONNECTED:
                    fprintf(pc,"WIFI_CONNECTED\r\n");
                    break;
                case WIFI_GOT_IP:
                    fprintf(pc,"WIFI_GOT_IP\r\n");
                    ESP32_init(FALSE);//sin hw_reset
                    wifi_ready = true;
                    break;
                case WIFI_DISCONNECTED:
                    fprintf(pc,"WIFI_DISCONNECTED\r\n");
                    break;
                case WIFI_CONNECTION_TIMEOUT:
                    fprintf(pc,"WIFI_CONNECTION_TIMEOUT\r\n");
                    break;
                case WIFI_WRONG_PASSWORD:
                    fprintf(pc,"WIFI_WRONG_PASSWORD\r\n");
                    break;
                case WIFI_CANNOT_FIND_AP:
                    fprintf(pc,"WIFI_CANNOT_FIND_AP\r\n");
                    break;
                case WIFI_CONNECTION_FILED:
                    fprintf(pc,"WIFI_CONNECTION_FILED\r\n");
                    break;
                default:
                    fprintf(pc,"UNKNOWN\r\n");
                    break;
            }
            
            break;
        case TCP_EVENT:
            fprintf(pc,"[TCP_EVENT]\r\n"); 
            fprintf(pc," >>");
            switch(tcp.response){
                case RECEIVE_NETWORK_DATA:
                    fprintf(pc,"[RECEIVE_NETWORK_DATA]\r\n");                               
                    fprintf(pc, " >>Data available %d\r\n",tcp.data_available);
                    fprintf(pc, " >>Data length %ld\r\n",tcp.rcv_data_len);
                    if(at_config.end_line){  //define si recibo bytes o strings
                        fprintf(pc," >>Data: [%s]\r\n",tcp.rcv_data);
                    }else{
                        fprintf(pc," >>Data: [");
                        int i;
                        for (i = 0; i < tcp.rcv_data_len; i++) {
                            fprintf(pc,"%X",tcp.rcv_data[i]);
                        }
                        fprintf(pc,"]\r\n");
                    }
                    break;
                case CONNECT:
                    fprintf(pc,"[CONNECT]\r\n");  
                    server_connected = true;
                    break;
                case CLOSED:
                    fprintf(pc,"[CLOSED]\r\n");  
                    break;
            }                                            
            break;
        case ESP_EVENT:
            fprintf(pc,"[ESP_EVENT]\r\n");
            break;
        default:
            fprintf(pc,"[UNKNOWN_EVENT ??]\r\n");
            break;
         
    }
}

void check_tcp_status(tcp_connection_status res){
	fprintf(pc,"[STATUS] ");
	switch (res)
	{
	case ESP_CONNECTED_TO_AP:
		fprintf(pc,"ESP_CONNECTED_TO_AP\r\n");
		break;
	case ESP_HAS_CREATED_TRANSMISSION:
		fprintf(pc,"ESP_HAS_CREATED_TRANSMISSION\r\n");
		break;

	case TCP_TRANSMISSION_DISCONNECTED:
		fprintf(pc,"TCP_TRANSMISSION_DISCONNECTED\r\n");
		break;
	case ESP_DOES_NOT_CONNECTED:
		fprintf(pc,"ESP_DOES_NOT_CONNECTED\r\n");
		break;
	default:
		fprintf(pc,"UNKNOWN_STATUS TCP\r\n");
		break;
	}
}
void wifi_process(void);
float get_temperature(void);

void main(void){

	set_tris_a(0b00000001);
	set_tris_b(0b00000001);
	set_tris_c(0b10000000);
	set_tris_d(0b10000000);
	set_tris_e(0b000);
	setup_timer_0(RTCC_INTERNAL|RTCC_DIV_16);		//210 ms overflow
	enable_interrupts(GLOBAL);
	enable_interrupts(INT_TIMER0);
	enable_interrupts(INT_RDA2);
	output_low(LED1);
	delay_ms(1000);
    fprintf(pc,"ESP32 MQTT WiFi example\r\n");
    
    setup_adc_ports(sAN0);
    set_adc_channel(0);
    setup_adc(ADC_CLOCK_DIV_32);
    radio_reset();
    mqttCredentials(&ubidots_token,&ubidots_pass,&mac); //Configura en variables los datos para conexion
    //wifi_process();
    
	
	while(true){
        esp32_event = ESP32_event_available(BYTE_TYPE);
        
        if(esp32_event != NULL_EVENT){
           check_ESP32_event(esp32_event);
           if (esp32_event == TCP_EVENT)
           {
               if(mqttPacketAvailable()){
                   if (mqttCheckData()==MQTT_CTRL_PUBLISH)
                   {
                       char reqTopic[50];
                       char reqPayload[50];
                       int payloadLength;
                       mqttReadPacket(&reqTopic,&reqPayload,&payloadLength);
                       fprintf(pc, "Topico recibido: %s\r\n",reqTopic );
                       fprintf(pc, "Payload recibido: %s\r\n",reqPayload );
                       fprintf(pc, "Length: %d\r\n",payloadLength );
                   }
                   
               }
           }
           
        }
        if(wifi_ready){
            delay_ms(2000);
            wifi_ready= false;
			if (mqttConnect(&broker,port)) {
				fprintf(pc, "Conectado a: %s\r\n",broker );
				delay_ms(1000);
				sprintf(topicIn,"test");
				fprintf(pc, "Suscribiendo a: %s\r\n",topicIn );
				if (mqttSubscribe(&topicIn)) {
					fprintf(pc, "OK\r\n" );		
                    KEEPALIVE = true;
                    delay_ms(2000);
				}

			}
                                              
        }
        if(mqtt.connected){
            if(KEEPALIVE){
                KEEPALIVE = FALSE;
                if(mqttSendPing()){
                    fprintf(pc, "Sigo online\r\n" );
                }
            }
            if (SEND_DATA){
                SEND_DATA = FALSE;
                fprintf(pc,"Enviando dato...\r\n");
                sprintf(topicOut,"/v1.6/devices/%s",mac);
                temperature = get_temperature();
                sprintf(payload,"{\"temp\":%.2f}",temperature);
                if (mqttPublish(&topicOut,&payload,strlen(payload))){
                    fprintf(pc,"Enviado correctamente\r\n");
                }
                
            }
            
        }
       

	}
}



void wifi_process(void){
    if (esp32_init())
	{
		fprintf(pc,"Modulo OK\r\n");
	}else{
		fprintf(pc,"modulo no responde\r\n");
	}
  
    wifi_mode(WIFI_STATION);
    wifi_auto_connect(TRUE); // cambiar dependiendo el modo de conexion deseado, previo a un reset
    if(wifi_connect(ssid,pass)){
		fprintf(pc,"[MAIN] Conectado!!..\r\n");
		fprintf(pc,"IP address: %s\r\n",wifi.ip);
	}else{
        fprintf(pc,"[ERROR] Error en conexion!!..\r\n");
        check_ESP32_event(wifi.status);
        fprintf(pc,"[ERROR] Please reset\r\n");
        while(true);
	}

	wifi_get_info(wifi.ip,wifi.gateway,wifi.netmask);
	fprintf(pc,"[MAIN] IP: %s\r\n",wifi.ip);
	fprintf(pc,"[MAIN] GW: %s\r\n",wifi.gateway);
	fprintf(pc,"[MAIN] NM: %s\r\n",wifi.netmask);
	tcp_get_status();
	check_tcp_status(tcp.status);
	
}

float get_temperature(void){
    nbitsADC = read_adc();
    float temp = (nbitsADC*3.25) / 1023.0;
    
    //float temp = nbitsADC * 0.48828125;
    return temp * 100.0;
}