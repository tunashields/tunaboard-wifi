/*
 * Project: TunaBoard-Wifi MQTTESP32
 * Description: Ejemplo para usar MQTT con ESP32
 * Author: Arturo Gasca
 * Date:  Julio-2019
 * Development: Tuna Shields
 * 

 * 
 *
 */

#include <tunaboard/tunamega_debug.h>
/*#include <18F67K40.h>
#device adc=10
#use delay(crystal=20mhz)
//#include <tunaboard/tunaone.h>*/
#use rs232(baud=115200,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8,stream=pc,restart_wdt)


#define RADIO_RST   PIN_E4
#define RADIO_RX    PIN_G2
#define RADIO_TX    PIN_G1
#define RADIO_BAUD  115200
#define DEBUG       0
#define RADIO_SERIAL_SOURCE RADIO_INT_RDA2

#define ESP_ENABLE PIN_E5

#include <string.h>
#include <stdlib.h>
#include <stdlibm.h>
#include "../AT/AT.h"
#include "../ESP/ESP.h"
#include "../http/http.h"
#include "../ubidots/ubidots.h"

#define DHT_PIN		PIN_A0
#define DHT_TYPE	DHT22
long t, h;
#include <tuna-lib/DHT.c>

char ssid[] = {"INFINITUM9159_2.4"};
char pass[] = {"4PwyP2nkRJ"};
//char ssid[] = {"INFINITUM1155"};
//char pass[] = {"37YHXC3nzs"};
//char ssid[] = {"vialactea"};
//char pass[] = {"bobyestaenmarte"};
//char ssid[] = {"HUAWEI P smart"};
//char pass[] = {"arturo123456"};




//char server[] = {"192.168.1.73"};
//int16 port = 10000;
char server[] = {"things.ubidots.com"};
int16 port = 80;
//char broker[] = {"74.208.169.198"};
//int16 port = 1884;


//char mac [] = {"00204AEA8B2E"}; //mqtt
char mac [] = {"00204AEA8B3E"}; //http


char ubidots_token[] = {"BBFF-GB9ZTrFXGR8Neqqls9JT5GPYymrNAo"};
 char respuesta[100];
 int16 status;

#define POLLING_TIME 30
int16 segundos = 0;
int16 cuenta = 0;


BOOLEAN SEND_DATA =true;

event_type esp32_event;

int16 nbitsADC;
float temperature;

#INT_TIMER0
void  TIMER0_isr(void){
 cuenta++;
 if(cuenta >=5){
     cuenta =0;
	 segundos++;
     
     if (segundos >=POLLING_TIME) {
         segundos = 0;
         SEND_DATA = true;
     }

   }
}


/*#INT_RDA2
void radio_isr(){
	radio_receive();
}*/
boolean wifi_ready = false;
boolean server_connected = false;

void check_ESP32_event(enum event_type incomming_event){
    
	switch(incomming_event){
        case WIFI_EVENT:
            fprintf(pc,"[WIFI_EVENT]\r\n");
            fprintf(pc," >>");
            switch (wifi.status){
                
                case WIFI_CONNECTED:
                    fprintf(pc,"WIFI_CONNECTED\r\n");
                    break;
                case WIFI_GOT_IP:
                    fprintf(pc,"WIFI_GOT_IP\r\n");
                    ESP_init(FALSE);//sin hw_reset
                    wifi_ready = true;
                    break;
                case WIFI_DISCONNECTED:
                    fprintf(pc,"WIFI_DISCONNECTED\r\n");
                    break;
                case WIFI_CONNECTION_TIMEOUT:
                    fprintf(pc,"WIFI_CONNECTION_TIMEOUT\r\n");
                    break;
                case WIFI_WRONG_PASSWORD:
                    fprintf(pc,"WIFI_WRONG_PASSWORD\r\n");
                    break;
                case WIFI_CANNOT_FIND_AP:
                    fprintf(pc,"WIFI_CANNOT_FIND_AP\r\n");
                    break;
                case WIFI_CONNECTION_FILED:
                    fprintf(pc,"WIFI_CONNECTION_FILED\r\n");
                    break;
                default:
                    fprintf(pc,"UNKNOWN\r\n");
                    break;
            }
            
            break;
        case TCP_EVENT:
            fprintf(pc,"[TCP_EVENT]\r\n"); 
            fprintf(pc," >>");
            switch(tcp.response){
                case RECEIVE_NETWORK_DATA:
                    fprintf(pc,"[RECEIVE_NETWORK_DATA]\r\n");                               
                    fprintf(pc, " >>Data available %d\r\n",tcp.data_available);
                    fprintf(pc, " >>Data length %ld\r\n",tcp.rcv_data_len);
                    if(at_config.end_line){  //define si recibo bytes o strings
                        fprintf(pc," >>Data: [%s]\r\n",tcp.rcv_data);
                    }else{
                        fprintf(pc," >>Data: [");
                        int i;
                        for (i = 0; i < tcp.rcv_data_len; i++) {
                            fprintf(pc,"%X",tcp.rcv_data[i]);
                        }
                        fprintf(pc,"]\r\n");
                    }
                    break;
                case CONNECT:
                    fprintf(pc,"[CONNECT]\r\n");  
                    server_connected = true;
                    break;
                case CLOSED:
                    fprintf(pc,"[CLOSED]\r\n");  
                    break;
            }                                            
            break;
        case ESP_EVENT:
            fprintf(pc,"[ESP_EVENT]\r\n");
            break;
        default:
            fprintf(pc,"[UNKNOWN_EVENT ??]\r\n");
            break;
         
    }
}

void check_tcp_status(tcp_connection_status res){
	fprintf(pc,"[STATUS] ");
	switch (res)
	{
	case ESP_CONNECTED_TO_AP:
		fprintf(pc,"ESP_CONNECTED_TO_AP\r\n");
		break;
	case ESP_HAS_CREATED_TRANSMISSION:
		fprintf(pc,"ESP_HAS_CREATED_TRANSMISSION\r\n");
		break;

	case TCP_TRANSMISSION_DISCONNECTED:
		fprintf(pc,"TCP_TRANSMISSION_DISCONNECTED\r\n");
		break;
	case ESP_DOES_NOT_CONNECTED:
		fprintf(pc,"ESP_DOES_NOT_CONNECTED\r\n");
		break;
	default:
		fprintf(pc,"UNKNOWN_STATUS TCP\r\n");
		break;
	}
}
void wifi_process(void);
float get_temperature(void);
void http_post_config(void);
void check_wifi_status(void);
void construyeJson(void);

void main(void){

	set_tris_a(0b00000001);
	set_tris_b(0b00000001);
	set_tris_c(0b10000000);
	set_tris_d(0b10000000);
	set_tris_e(0b000);
	setup_timer_0(RTCC_INTERNAL|RTCC_DIV_16);		//210 ms overflow
	
	enable_interrupts(INT_TIMER0);
	enable_interrupts(INT_RDA2);
    enable_interrupts(GLOBAL);
	output_low(LED1);
    DHT_init();
	delay_ms(1000);
    fprintf(pc,"ESP8266 HTTP example\r\n");
	http_post_config();
    
    //setup_adc_ports(sAN0);
    //set_adc_channel(0);
    //setup_adc(ADC_CLOCK_DIV_32);
    //radio_reset();
/*
    //ubidots_init(ubidots_token);

    double dot_temp = 45.5;
    double dot_hum = 85.5;
    double dot_lux = 698.95;

    UbidotsCollection *dots = ubidots_collection_init(5);
    char variable_id_char[20];
    
    strcpy(variable_id_char,"Temperatura");
    ubidots_collection_add(dots,variable_id_char,dot_temp);
    strcpy(variable_id_char,"Humedad");
    ubidots_collection_add(dots,variable_id_char,dot_hum);
    strcpy(variable_id_char,"Luz");
    ubidots_collection_add(dots,variable_id_char,dot_lux);
    strcpy(variable_id_char,"wats");
    ubidots_collection_add(dots,variable_id_char,dot_hum);
    strcpy(variable_id_char,"power");
    ubidots_collection_add(dots,variable_id_char,dot_lux);
    
    
    //fprintf(pc, "to save: %s\r\n",dots->variable_ids[0]);
    //fprintf(pc, "to save: %s\r\n",dots->variable_ids[1]);
    //fprintf(pc, "to save: %s\r\n",dots->variable_ids[2]);
    
    ubidots_collection_save(dots,respuesta);
    
    fprintf(pc,"json to ubidots: %s\r\n",respuesta);

    


    while(TRUE);
    */
    wifi_process();
    
	//while(true);
    
	while(true){
        esp32_event = ESP_event_available(CHAR_TYPE);
        
        if(esp32_event != NULL_EVENT){
           check_ESP32_event(esp32_event);     
        }
        if(wifi.last_event == WIFI_GOT_IP){
            check_wifi_status();
        }
        if(wifi.status == WIFI_GOT_IP){
            if (SEND_DATA)
            {
                
                SEND_DATA = FALSE;
				construyeJson();
                if (HTTP_POST(&respuesta,&status)) {  //Funcion que hace el POST, regresa el puntero de la respues del server
                    fprintf(pc, "[MAIN] POST EXITOSO\r\n" );
                    fprintf(pc, "[MAIN] STATUS CODE: %ld\r\n",status );
                    fprintf(pc, "[MAIN] RESPUESTA SERVER: %s\r\n",respuesta );

                }else{
                    fprintf(pc, "[MAIN] POST FALLIDO\r\n" );
                    tcp_get_status();
                    if(tcp.status == ESP_DOES_NOT_CONNECTED){
                        fprintf(pc, "[MAIN] ESP no conectado a la red\r\n" );
                        wifi.status = WIFI_DISCONNECTED;
                        wifi_process();
                    }                                        
                }
            }            
        }				 
	}
}



void wifi_process(void){
    if (ESP_init(1))
	{
		fprintf(pc,"Modulo OK\r\n");
	}else{
		fprintf(pc,"modulo no responde\r\n");
        while(TRUE);
	}
  
    wifi_mode(WIFI_STATION);
    wifi_auto_connect(FALSe); // cambiar dependiendo el modo de conexion deseado, previo a un reset
    if(wifi_connect(ssid,pass)){
		fprintf(pc,"[MAIN] Conectado!!..\r\n");
		fprintf(pc,"IP address: %s\r\n",wifi.ip);
	}else{
        fprintf(pc,"[ERROR] Error en conexion!!..\r\n");
        check_ESP32_event(wifi.status);
        fprintf(pc,"[ERROR] Please reset\r\n");
        while(true);
	}
}
void check_wifi_status(void){
    wifi_get_info(wifi.ip,wifi.gateway,wifi.netmask);
	fprintf(pc,"[MAIN] IP: %s\r\n",wifi.ip);
	fprintf(pc,"[MAIN] GW: %s\r\n",wifi.gateway);
	fprintf(pc,"[MAIN] NM: %s\r\n",wifi.netmask);
	tcp_get_status();
	check_tcp_status(tcp.status);
}
/*float get_temperature(void){
    nbitsADC = read_adc();
    float temp = (nbitsADC*3.25) / 1023.0;
    
    //float temp = nbitsADC * 0.48828125;
    return temp * 100.0;
}*/

void http_post_config(void){
	strcpy(http.HOST,server);
  	sprintf(http.PATH,"/api/v1.6/devices/%s",mac);
  	sprintf(http.HEADERS,"X-Auth-Token:%s",ubidots_token);
  	strcpy(http.CONTENT_TYPE,"application/json");
}

 void construyeJson(void){
     if(DHT_Get_Values(&t,&h) == DHT_OK){
        fprintf(pc, "H: %3.1w%% T: %3.1wC\r\n", h,t);        
    }
    else{
        fprintf(pc, "SENSOR FAIL!!\r\n");
    }
     //temperature = get_temperature();
     sprintf(http.DATA,"{\"temp\":%3.1w,\"hum\":%3.1w}",t,h);
}
